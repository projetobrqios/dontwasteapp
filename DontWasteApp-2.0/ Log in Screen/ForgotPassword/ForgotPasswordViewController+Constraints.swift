//
//  ForgotPasswordVC+Constraints.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 27/09/21.
//

import UIKit

extension ForgotPasswordViewController {
    
    func setConstraints() {
        let margins = view.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            
            userImage.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
            userImage.topAnchor.constraint(equalTo: margins.topAnchor, constant: 45),
            userImage.widthAnchor.constraint(equalToConstant: 65),
            userImage.heightAnchor.constraint(equalToConstant: 70),
            
            text.topAnchor.constraint(equalTo: userImage.bottomAnchor, constant: 35),
            text.centerXAnchor.constraint(equalTo: margins.centerXAnchor),
            
            emailField.topAnchor.constraint(equalTo: text.bottomAnchor, constant: 35),
            emailField.leadingAnchor.constraint(equalTo: margins.leadingAnchor),
            emailField.trailingAnchor.constraint(equalTo: margins.trailingAnchor)
        ])
    }
}
