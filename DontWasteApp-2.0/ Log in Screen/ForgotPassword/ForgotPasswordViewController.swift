//
//  ForgotPasswordVC.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 27/09/21.
//

import UIKit
import FirebaseAuth

class ForgotPasswordViewController: BasicViewController {
    
    //MARK: - ATRIBUTES
    let messages = Messages()
    let userImage = Image().createDefaultImage(
        imageName: "cadastrousuario",
        alpha: 1,
        contentMode: .scaleAspectFit
    )
    let text = TextLabel1().createDefaultLabel(
        text: "Esqueci minha senha",
        size: 30,
        bold: false
    )
    let emailField = TextField().createDefaultTextField(
        placeholder: "E-mail",
        border: true,
        isPassword: false,
        autoCaps: false,
        leftSpace: true
    )
    private let sendButton = Button().createBottomButton(
        title: "Enviar",
        green: true
    )
    
    //MARK: - METHOD
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withRightButton: true)

        view.backgroundColor = .white

        emailField.keyboardType = .emailAddress

        view.addSubview(userImage)
        view.addSubview(text)
        view.addSubview(emailField)
        view.addSubview(sendButton)
        
        let tap = UITapGestureRecognizer( target: view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
        setConstraints()
        
        if FirebaseAuth.Auth.auth().currentUser == nil {
            
            self.emailField.becomeFirstResponder()
        }
    }
    //MARK: - toolbar button
    override func addRightButton() -> UIBarButtonItem {
        //item
        let rightItem = UIBarButtonItem(customView: sendButton)
        //button target
        sendButton.addTarget(
            self,
            action: #selector(sendEmail),
            for: .touchUpInside
        )
        return rightItem
    }
    //MARK: - validate and send email
    @objc func sendEmail(){
        
        guard let email = self.emailField.text, !email.isEmpty
        else {
            
            Alert(controller: self).setAlert(
                title: self.messages.alertEmailErrorTitle,
                message: self.messages.alertEmailErrorContent
            )
            return
        }
        Auth.auth().sendPasswordReset(withEmail: email) {
            
            error in
            guard error == nil else {
                
                Alert(controller: self).setAlert(
                    title: self.messages.alertForgotEmailDoesNotExistTitle,
                    message: self.messages.alertForgotEmailDoesNotExistContent
                )
                return
            }
            Alert(controller: self).setAlert(
                title: self.messages.alertForgotEmailSentTitle,
                message: self.messages.alertForgotEmailSentContent
            )
        }
    }
}
