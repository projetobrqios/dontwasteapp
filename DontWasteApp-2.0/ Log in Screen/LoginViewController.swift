//
//  LoginViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 31/08/21.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class LoginViewController: BasicViewController {
    
    //MARK: - ATRIBUTES
    private let loginView = LoginView()
    private var user = User()
    private var inputEmail = ""
    private var inputPassword = ""
    private var message = Messages()
    private let logButton = Button()
        .createBottomButton(title: "LOGAR", green: true)
    
    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = ""
        setNavigationBar(true)
        setToolBar(true, withRightButton: true)
        
        self.view = loginView
        loginView.setView()
        
        //Button targets
        loginView.forgotPassButton.addTarget(
            self,
            action: #selector(goPassRecover),
            for: .touchUpInside
        )
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(UIInputViewController.dismissKeyboard)
        )
        view.addGestureRecognizer(tap)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if FirebaseAuth.Auth.auth().currentUser == nil {
            loginView.emailField.becomeFirstResponder()
        }
    }
    //MARK: - interface actions
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc private func goPassRecover() {
        let forgot1VC = ForgotPasswordViewController()
        navigationController?
            .pushViewController(forgot1VC, animated: true)
    }
    //MARK: - toobal button
    override func addRightButton() -> UIBarButtonItem {
        //item
        let rightItem = UIBarButtonItem(customView: logButton)
        //button target
        logButton.addTarget(
            self,
            action: #selector(goLoggedScreen),
            for: .touchUpInside
        )
        return rightItem
    }
    //MARK: - firebase
    @objc private func goLoggedScreen() {
        print("===============LOG pressed")
        guard let email = self.loginView.emailField.text,
              !email.isEmpty,
              let password = self.loginView.passwordField.text,
              !password.isEmpty
        else {
                  Alert(controller: self)
                .setAlert(
                    title: message.alertLoginIsBlankTitle,
                    message: message.alertLoginIsBlankContent
                  )
            print(">>>>>>>>>>>um dos campos está vazio")
                  return
              }
        
        // SPRINT 5 / THIAGO //////////////////////////////////////////////////////////////////////////////////////

        AuthManager().firebaseLogin(email: email, password: password) { result in
            if result {
                DatabaseManager().getUserProfileData()
                let loggedHomeVC = LoggedHomeViewController()
                self.navigationController?.pushViewController(loggedHomeVC, animated: true)
            } else {
                Alert(controller: self).setAlert(title: self.message.alertLoginWritingErrorTitle,
                                                 message: self.message.alertLoginWritingErrorContent)
            }
        }
        
        // //////////////////////////////////////////////////////////////////////////////////////////////////////
        
    }
    
    func setUser(){
        self.user.setLogin(self.inputEmail, self.inputPassword)
    }

}
