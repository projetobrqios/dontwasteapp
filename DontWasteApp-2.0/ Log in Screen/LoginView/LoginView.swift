//
//  Login1View.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/09/21.
//

import UIKit

class LoginView: UIView {
    
    //MARK: - ATRIBUTES
    let messages = Messages()
    
    let userImage = Image().createDefaultImage(
        imageName: "cadastrousuario",
        alpha: 1,
        contentMode: .scaleAspectFit
    )
    
    let emailField = TextField().createDefaultTextField(
        placeholder: "E-mail",
        border: true,
        isPassword: false,
        autoCaps: false,
        leftSpace: true)
    
    let passwordField = TextField().createDefaultTextField(
        placeholder: "Senha",
        border: true,
        isPassword: true,
        autoCaps: false,
        leftSpace: true)
    
    let forgotPassButton = Button()
        .createTextButton(text: "esqueci minha senha", size: 16)
    
    //MARK: - METHODS
    func setView() {
        self.backgroundColor = .white
        
        emailField.keyboardType = .emailAddress
        passwordField.keyboardType = .numberPad
        
        self.addSubview(userImage)
        self.addSubview(emailField)
        self.addSubview(passwordField)
        self.addSubview(forgotPassButton)
        
        setConstraints()
    }
}
