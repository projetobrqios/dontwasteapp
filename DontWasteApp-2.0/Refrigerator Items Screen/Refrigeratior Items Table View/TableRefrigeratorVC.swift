import UIKit
import FirebaseAuth
import FirebaseDatabase

class TableRefrigeratorVC: UITableViewController {
    
    
    //MARK: - ATRIBUTES
    let cellId = "cellId"
    let cathegory = "fruitsAndVegetables"
    var foodItens: [Food]  = []
    var timer = Timer()
    
    
    //MARK: - view life cycle
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadItems()
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.register(
            MyCell.self,
            forCellReuseIdentifier: cellId
        )
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.foodItens.removeAll()
    }
    
    
    
    @objc func loadItems(){
        
        print("22222222222222222222")
        
        self.timer.invalidate()
        self.foodItens.removeAll()
        
        let database = Database.database().reference()
        let usuarios = database.child("users/\(AuthManager().getLoggedUserUid())/refrigerator/\(cathegory)")
        usuarios.observe(DataEventType.childAdded, with: { (snapshot) in
            let dados = snapshot.value as? NSDictionary
            let fruta = Food()
            fruta.nameFood = dados?["foodName"] as! String
            fruta.amount = dados?["amount"] as! Int

            self.foodItens.append(fruta)
            self.tableView.reloadData()

        })
    }


    
    func submit(_ answer: String) {
        var counter = 0
        for food in foodItens {
            if food.nameFood == answer || answer == "" {
                counter += 1
             } else {
                counter -= 0
            }
        }
        
        if counter >= 1 {
            Alert.init(controller: self).setAlert(
                title: "Item Não Adicionado",
                message: "Não pode ser um Item Repetido nem um Item Sem Título")
        } else {
            let nameFood = answer
            let amount = 1
            addItem(foodName: nameFood, amount: amount)
        }
    }
    
    
    func addItem(foodName: String, amount: Int) {
        
        print("111111111111111111111")
        
        let database = Database.database().reference()
        let usuarios = database.child("users/\(AuthManager().getLoggedUserUid())/refrigerator/\(cathegory)")
        let newFood = [ "foodName": foodName,
                        "amount": 1] as [String : Any]
        usuarios.child(foodName).setValue(newFood)
        
        timer = Timer.scheduledTimer(timeInterval: 0.3, target: self, selector: #selector(loadItems), userInfo: nil, repeats: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    override func tableView(
        _ tableView: UITableView,
        cellForRowAt indexPath: IndexPath
    ) -> UITableViewCell
    {
        let cell = tableView
            .dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MyCell
        let currentLastItem = foodItens[ indexPath.row ]
        cell.foodItem = currentLastItem
        tableView.allowsSelection = false
        return cell
    }
    
    
    override func tableView(
        _ tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int
    {
        return foodItens.count
    }
    
    
    override func tableView(
        _ tableView: UITableView,
        heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 50
    }
    
    
    
}
