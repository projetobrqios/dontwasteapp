//
//  CustomTVCell.swift
//  DontWasteApp-2.0
//
//  Created by user on 04/11/21.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

protocol ProductCellDelegate {
    func increaseNumber(cell: MyCell,number : Int)
    func decreaseNumber(cell: MyCell,number : Int)
}

class MyCell: UITableViewCell
{
    //MARK: - ATRIBUTES
    var delegate : ProductCellDelegate?
    let minValue = 0
    var foodItem : Food?
    {
        didSet
        {
            guard let numero = foodItem?.amount else { return }
            let numeroConv = "\(numero)"
            productNameLabel.text = foodItem?.nameFood
            quantityField.text = numeroConv
        }
    }
    var buttonTapCallback: () -> ()  = { }
    private let productNameLabel : UILabel =
    {
        let lbl = UILabel()
        lbl.textColor = .black
        lbl.font = UIFont.boldSystemFont(ofSize: 16)
        lbl.textAlignment = .left
        return lbl
    }()
    private let plusButton: UIButton =
    {
        let btn = UIButton(type: .custom)
        btn.setImage(#imageLiteral(resourceName: "+"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    private let minusButton : UIButton =
    {
        let btn = UIButton(type: .custom)
        btn.setImage(#imageLiteral(resourceName: "-"), for: .normal)
        btn.imageView?.contentMode = .scaleAspectFit
        return btn
    }()
    var quantityField : UILabel =  {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16)
        label.layer.cornerRadius = 4
        label.layer.borderWidth = 1
        label.layer.borderColor = UIColor(
            red: 94/255,
            green: 186/255,
            blue: 168/255,
            alpha: 1
        ).cgColor
        label.textAlignment = .center
        label.text = "0"
        label.textColor = .black
        return label
    }()
    
    //MARK: - METHODS
    @objc func decreaseFunc()
    {
        changeQuantity(by: -1)
        updteFirebaseNumber()

    }
    
    @objc func increaseFunc()
    {
        changeQuantity(by: 1)
        updteFirebaseNumber()
        
    }
    
    
    
    func changeQuantity(by amount: Int)
    {
        var quality = Int(quantityField.text!)!
        quality += amount
        if quality < minValue
        {
            quality = 1
            quantityField.text = "0"
        } else
        {
            quantityField.text = "\(quality)"
        }
        delegate?.decreaseNumber(cell: self, number: quality)
        
    }
    
    
    @objc func updteFirebaseNumber(){
        
        let database = Database.database().reference()
        let usuarios = database.child("users/\(AuthManager().getLoggedUserUid())/refrigerator/\(TableRefrigeratorVC().cathegory)")
        usuarios.observeSingleEvent(of: .value, with: { (snapshot) in

            for data in snapshot.children.allObjects as! [DataSnapshot] {
                if let data = data.value as? [String: Any] {
                    if let itemName = data["foodName"] as? String {
                        if itemName == self.productNameLabel.text {

                            let database = Database.database().reference()
                            let usuarios = database.child("users/\(AuthManager().getLoggedUserUid())/refrigerator/\(TableRefrigeratorVC().cathegory)/\(itemName)")
                            let newAmount = Int(self.quantityField.text!)!
                            usuarios.updateChildValues(["amount": newAmount])
                        }
                    }
                }
            }
        })
    }
    
    
    
    override init(
        style: UITableViewCell.CellStyle,
        reuseIdentifier: String?)
    {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(productNameLabel)
        addSubview(plusButton)
        addSubview(minusButton)
        addSubview(quantityField)
        contentView.isUserInteractionEnabled = true
        productNameLabel.anchor(
            top: topAnchor,
            left: leftAnchor,
            bottom: nil,
            right: nil,
            paddingTop: 20,
            paddingLeft: 50,
            paddingBottom: 0,
            paddingRight: 0,
            width: frame.size.width / 2,
            height: 0,
            enableInsets: false
        )
        let stackView = UIStackView(
            arrangedSubviews: [ minusButton,quantityField,plusButton ]
        )
        stackView.distribution = .fillEqually
        stackView.axis = .horizontal
        stackView.spacing = 10
        addSubview(stackView)
        stackView.anchor(
            top: topAnchor,
            left: productNameLabel.rightAnchor,
            bottom: bottomAnchor,
            right: rightAnchor,
            paddingTop: 10,
            paddingLeft: 15,
            paddingBottom: 10,
            paddingRight: 70,
            width: 100,
            height: 50,
            enableInsets: false
        )
        minusButton.addTarget(
            self,
            action: #selector(decreaseFunc),
            for: .touchUpInside
        )
        plusButton.addTarget(
            self,
            action: #selector(increaseFunc),
            for: .touchUpInside
        )
    }
    required init?(coder aDecoder: NSCoder)
    {
        fatalError("init(coder:) has not been implemented")
    }   
}
