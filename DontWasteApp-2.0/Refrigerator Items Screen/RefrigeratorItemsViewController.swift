//
//  RefrigeratorItemsViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 09/11/21.
//

import UIKit

class RefrigeratorItemsViewController: LoggedViewController
{
    //MARK: - ATRIBUTES
    let itemsTVC = TableRefrigeratorVC()
    let grayBox = Button().createNewButton(title: "   + NOVO ITEM")
    
    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true, withRightButton: true)
        setViewWithBanner(
            withCathegory: true,
            cathegoryName: "GELADEIRA",
            cathegoryImageName: "geladeira_aberta",
            withSubCathegory: true,
            subcathegoryName: "FRUTAS, LEG. E VERDURAS",
            subcathegoryImageName: "frutas"
        )
        self.addChild(itemsTVC)// -- instancia a View Controller da Table View
        self.view.addSubview(itemsTVC.view) // -- instancia a view da Table View
        
        self.view.addSubview(grayBox)
        setupConstraints() // -- seta as constraints nessa tela
        itemsTVC.didMove(toParent: self) // -- called after the view controller is added or removed from a container view controller.
        
        grayBox.addTarget(
            self,
            action: #selector(showAlert),
            for: .touchUpInside
        )
    }
    //toolbar right button
    override func addRightButton() -> UIBarButtonItem {
        //item
        let registeredFoodButton = Button().createBottomButton(
            title: "  ALIMENTOS CADASTRADOS  ",
            autoSize: false,
            green: true
        )
        let rightItem = UIBarButtonItem(customView: registeredFoodButton)
        //constraint
        NSLayoutConstraint.activate([
            registeredFoodButton.heightAnchor
                .constraint(equalToConstant: 40)
        ])
        //target
        registeredFoodButton.addTarget(
            self,
            action: #selector(goRegisteredFood),
            for: .touchUpInside
        )
        return rightItem
    }
    @objc func goRegisteredFood()
    {
        let registeredFoodVC = BlankLoggedViewController()
        navigationController?
            .pushViewController(registeredFoodVC, animated: true)
    }
    @objc private func showAlert()
    {
        let alert = UIAlertController(
            title: "Adicionar item",
            message: "Digite um item abaixo",
            preferredStyle: .alert
        )
        alert.addTextField()
        let submitAction = UIAlertAction(
            title: "Continue",
            style: .default
        )
        { [ weak itemsTVC, weak alert ] _ in
            guard let answer = alert?.textFields?[0].text else { return }
            itemsTVC?.submit(answer.uppercased())
        }
        alert.addAction(submitAction)
        alert.addAction(UIAlertAction(
            title: "Cancel",
            style: .cancel,
            handler: nil)
        )
        present(alert, animated: true)
    }
    
    func setupConstraints()
    {
        itemsTVC.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [
                itemsTVC.view.topAnchor.constraint(
                    equalTo: subInternalView.bottomAnchor, constant: spaceHeight
                ),
                itemsTVC.view.leadingAnchor.constraint(
                    equalTo: view.safeAreaLayoutGuide.leadingAnchor
                ),
                itemsTVC.view.trailingAnchor.constraint(
                    equalTo: view.safeAreaLayoutGuide.trailingAnchor
                ),

                itemsTVC.tableView.bottomAnchor.constraint(
                    equalTo: grayBox.topAnchor
                ),
                
                grayBox.bottomAnchor.constraint(
                    equalTo: self.view.layoutMarginsGuide.bottomAnchor
                ),
                grayBox.heightAnchor.constraint(
                    equalToConstant: 40
                ),
                grayBox.widthAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.widthAnchor, constant: -5
                ),
                grayBox.centerXAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.centerXAnchor
                ),
            ]
        )
    }
}
