//
//  ValidaCadastro.swift
//  DontWastAppPaThi
//
//  Created by Thiago de Angele on 17/08/21.
//

import Foundation

struct RegisterValidation {
    
    func validateName(name: String) -> Bool {
        if name.count > 200 {
            return false
        } else if name == "" {
            return false
        } else {
            return true
        }
    }
    
    func validateEmptyField(field: String) -> Bool{
        if field == "" {
            return false
        } else {
            return true
        }
    }
    
    func validateEmail(email: String) -> Bool {
        if validateEmptyEmail(email: email) {
            if validateValidEmail(email: email) {
                return true
            } else {
                return false
            }
        } else {
            return false
        }
    }

    func validateEmptyEmail(email: String) -> Bool {
        if email == "" {
            return false
        } else {
            return true
        }
    }

    func validateValidEmail(email: String) -> Bool {
        let emailCharacter = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailCharacter)
        let result = emailTest.evaluate(with: email)
        return result
    }
    
    func validatePassword(password: String) -> Bool {
        if password.count < 6 {
            return false
        } else if password.count > 8 {
            return false
        } else if (Int(password) == nil) {
            return false
        } else {
            return true
        }
    }
}
