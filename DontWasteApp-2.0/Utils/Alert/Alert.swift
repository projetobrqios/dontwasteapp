//
//  Alerta.swift
//  DontWastAppPaThi
//
//  Created by Thiago de Angele on 16/08/21.
//

import UIKit

class Alert{
    //MARK: - Atributes
    let controller: UIViewController
    
    //MARK: - Methods
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    func setAlert(title: String, message: String){
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: "fechar", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        
        controller.present(alert, animated: true, completion: nil)
    }
}
