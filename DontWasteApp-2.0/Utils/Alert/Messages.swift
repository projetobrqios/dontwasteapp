//
//  Mensagens.swift
//  DontWastAppPaThi
//
//  Created by Thiago de Angele on 17/08/21.
//

import Foundation

struct Messages{
    
    //MARK: - Registration 1
    let alertEmailErrorTitle = "Erro no campo E-mail"
    let alertEmailErrorContent = "O campo E-mail não pode ficar em branco. Insira um e-mail válido"
    
    let alertPasswordErrorTitle = "Erro no campo Senha"
    let alertPasswordErrorContent = "O campo Senha não pode ficar em branco, deve ter entre 6 e 8 caracteres e apenas números."
    
    //MARK: - Registration 2
    let alertNameErrorTitle = "Erro no campo Nome"
    let alertNameErrorContent = "O campo NOME não pode ficar vazio nem ter mais que 200 caracteres."
    
    let alertHowManyRoomatesErrorTitle = "Erro no campo 'Quantidade'"
    let alertHowManyRoomatesErrorContent = "Quantas pessoas residem em sua casa?"
    
    let alertDateErrorTitle = "Erro no campo 'Data'"
    let alertDateErrorContent = "Informe qual a data da sua última compra no período de 12 meses."
    
    let alertHowOftenShopErrorTitle = "Erro no campo 'Frequência'"
    let alertHowOftenShopErrorContent = "Qual sua frequência de compras?"
    
    let alertRegistrationErrorTitle = "Algo deu Errado"
    let alertRegistrationErrorContent = "Seu e-mail está correto?\nTente fazer o cadastro novamente"
    
    //MARK: - LOGIN
    let alertLoginIsBlankTitle = "Campo vazio"
    let alertLoginIsBlankContent = "Preencha o campo que está vazio"
    let alertLoginWritingErrorTitle = "Campo incorreto"
    let alertLoginWritingErrorContent = "E-mail e/ou Senha está incorreta"
    
    //MARK: - FIREBASE
    let alertFirebaseAuthErrorTitle = "Houve um Erro"
    let alertFirebaseAuthErrorContent = "Tente Novamente"
    
    let alertFirebaseEmailAlreadyExistsTitle = "Este e-mail já está cadastrado"
    let alertFirebaseEmailAlreadyExistsContent = "Faça o login ou escolha 'Esqueci minha Senha'"
    
    //MARK: - FORGOTPASSWORD
    let alertForgotEmailDoesNotExistTitle = "Este e-mail não está cadastrado"
    let alertForgotEmailDoesNotExistContent = "Volte para a home e se cadastre"
    
    let alertForgotEmailSentTitle = "Senha resetada"
    let alertForgotEmailSentContent = "Email enviado com link para resetar sua senha. Não esqueça de checar o SPAM"
    
}
