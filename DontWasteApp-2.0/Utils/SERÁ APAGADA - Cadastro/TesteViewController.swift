//
//  TesteViewController.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 01/11/21.
//

import UIKit
import FirebaseDatabase

class TesteViewController: LoggedViewController {
    
    let addButton = Button().createTextButton(text: "+", size: 80, color: .green)
    let dellButton = Button().createTextButton(text: "-", size: 80, color: .red)
    var fruitAmount = 0
    var listDell: Int = 0
    var listAdd: Int = 0
    let amount = TextLabel1().createDefaultLabel(text: "0", size: 100, bold: true)
    
    override func viewWillAppear(_ animated: Bool) {
        fruitAmount = 0
        amountFruit()
        printAllFruits()
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
        
        title = "LOGGED > FRUTAS E LEGUMES"
        
        self.view.backgroundColor = .white
        
        addButton.addTarget(self, action: #selector(addFruit), for: .touchUpInside)
        dellButton.addTarget(self, action: #selector(dellFruit), for: .touchUpInside)
        
        self.view.addSubview(amount)
        self.view.addSubview(addButton)
        self.view.addSubview(dellButton)
        
        constraints()
    }
    
    func constraints(){
        amount.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        amount.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 200).isActive = true
        addButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        addButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor).isActive = true
        dellButton.topAnchor.constraint(equalTo: addButton.bottomAnchor, constant: 20).isActive = true
        dellButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
    }
    
    
    
    // MARK: -  RECUPERANDO UMA QUANTIDADE
    
    func amountFruit(){
        let caminho = Database.database().reference().child("users").child(AuthManager().getLoggedUserUid()).child("refrigerator")
            .child("fruitsAndVegetable").child("Melão").child("info")
        caminho.observe(.value) { snapshot in
            
            let dados = snapshot.value as? NSDictionary
            
            if let amountRec = dados?["amount"] as? Int {
                self.fruitAmount = amountRec
                self.amount.text = "\(self.fruitAmount)"
            } else {
                self.amount.text = "0"
            }
        }
    }
    
    
    // MARK: -  ACRESCENTANDO UM ITEM
    
    @objc func addFruit(){
        
        self.fruitAmount += 1
        self.amount.text = "\(self.fruitAmount)"
        
        let caminho = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/info")
        caminho.child("amount").setValue(self.fruitAmount)
        
        let caminho2 = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/add").childByAutoId()
        let timestamp = ServerValue.timestamp()
        let usersData = ["addAmount": 1,
                         "addDate": timestamp ] as [String : Any]
        
        caminho2.setValue(usersData)
        
        
        
        let transactionRef = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/add")
        transactionRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            self.listAdd = Int(snapshot.childrenCount)
            

        })
    }
    
    
    // MARK: -  APAGANDO UM ITEM
    
    @objc func dellFruit(){
        
        if self.fruitAmount > 0 {
            
            self.fruitAmount -= 1
            self.amount.text = "\(self.fruitAmount)"
            
            let caminho = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/info")
            caminho.child("amount").setValue(self.fruitAmount)
            
            let caminho2 = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/dell").childByAutoId()
            let timestamp = ServerValue.timestamp()
            let usersData = ["dellAmount": 1,
                             "delldate": timestamp ] as [String : Any]
            
            caminho2.setValue(usersData)
            
            
            
            
            let transactionRef = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable/Melão/dell")
            transactionRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                self.listDell = Int(snapshot.childrenCount)
                
                if snapshot.childrenCount > 0 {
                    for data in snapshot.children.allObjects as! [DataSnapshot] {
                        if let data = data.value as? [String: Any] {
                            if let delldate = data["delldate"] as? Double {
                                if let quantidade = data["dellAmount"] as? Int {
                                    print("DELETOU - Data: \(DatabaseManager().convertTimestamp(dataToConvert: delldate))   Quantidade: \(quantidade)")
                                }
                            }
                        }
                    }
                }
            })
        } else {
            //
        }
    }
    
    
    // MARK: -  LISTANDO UM NÓ
    func printAllFruits(){
        
        let transactionRef = Database.database().reference().child("users/\(AuthManager().getLoggedUserUid())/refrigerator/fruitsAndVegetable")
        transactionRef.observeSingleEvent(of: .value, with: { (snapshot) in
            
            for data in snapshot.children.allObjects as! [DataSnapshot] {
                let dados = data.key
                print("/////////////////// FRUTAS: \(dados)")
            }
            
        })
    }
    
 
 
    
    
}
