//
//  CreateLabel.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

class TextLabel1 {
    
    func createDefaultLabel(text: String, size: CGFloat = 17, bold: Bool) -> UILabel {
        let defaultLabel: UILabel = {
           let label = UILabel()
            label.text = text
            label.numberOfLines = 0
            label.textColor = .black
            
            if bold {
                label.font = UIFont(name: "Arial Bold", size: size)
            } else {
                label.font = UIFont(name: "Arial", size: size)
            }
            
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        return defaultLabel
    }
    
    func createOpenSansLabel(text: String, size: CGFloat, greenColor: Bool) -> UILabel {
        let openSansLabel: UILabel = {
            let label = UILabel()
            label.text = text
            label.font = UIFont(name: "OpenSans-Bold", size: size)
            label.textAlignment = .center
            
            if greenColor {
                label.textColor = UIColor.init(red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            } else {
                label.textColor = .black
            }
            
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
        }()
        
        return openSansLabel
    }
}
