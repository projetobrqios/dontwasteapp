//
//  CreateTextField.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

/*
 PLACEHOLDER - Texto dentro do TextField
 BORDER - Se true, aplica borda verde em torno do TextField
 ISPASSWORD - Se true, esconde o que está sendo digitado
 AUTOCAPS - Se true, deixa a primeira letra maiúscula
 LEFTSPACE - Adiciona um espaço interno à esquerda
*/

class TextField {
    
    func createDefaultTextField(placeholder: String = "",
                          border: Bool = false,
                          isPassword: Bool = false,
                          autoCaps: Bool = true,
                          leftSpace: Bool = true) -> UITextField {
        
        let defaultTextField: UITextField = {
            let field = UITextField()
            field.placeholder = placeholder
            field.backgroundColor = .white
            field.leftViewMode = .always
            field.translatesAutoresizingMaskIntoConstraints = false
            
            if leftSpace {
                field.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 0))
            }
            
            if border {
                field.layer.borderWidth = 1
                field.layer.borderColor = (CGColor.init(red: 94/255, green: 186/255, blue: 168/255, alpha: 1))
            }
            if isPassword {
                field.isSecureTextEntry = true
            }
            
            if autoCaps == false {
                field.autocapitalizationType = .none
            }
            
            return field
        }()
        
        defaultTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true

        return defaultTextField
    }
}
