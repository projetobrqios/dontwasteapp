//
//  MakeButton.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

class Button {
    
    func createImageButton(imageName: String) -> UIButton {
        let imageButton: UIButton = {
            let image = UIImage(named: imageName) as UIImage?
            let button  = UIButton(type: .system)
            button.setImage(image, for: .normal)
            button.tintColor = UIColor.init(displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        return imageButton
    }
    
    
    func createTextButton(text: String, size: CGFloat, color: UIColor = UIColor.black) -> UIButton {
        let textButton: UIButton = {
            let button  = UIButton(type: .system)
            button.setTitle(text, for: .normal)
            button.titleLabel?.font =  UIFont(name: "Arial", size: size)
            button.tintColor = color
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        return textButton
    }

    
    func createBottomButton(title: String, autoSize: Bool = true, green: Bool) -> UIButton {
        let primaryButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.titleLabel?.font = UIFont(name: "Arial Bold", size: 12)
            button.setTitleColor(UIColor.black, for: .normal)
            button.translatesAutoresizingMaskIntoConstraints = false
            
            if green {
                button.backgroundColor = UIColor.init(red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            } else {
                button.backgroundColor = UIColor.init(displayP3Red: 223/255, green: 213/255, blue: 214/255, alpha: 1)
            }
            return button
        }()
        
        if autoSize {
            if title.count < 10 {
                let width: CGFloat = 100
                primaryButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
                primaryButton.widthAnchor.constraint(equalToConstant: width).isActive = true
            } else {
                let width = CGFloat(title.count + 120)
                primaryButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
                primaryButton.widthAnchor.constraint(equalToConstant: width).isActive = true
            }
        } else {
            primaryButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        }
        return primaryButton
    }
   
    
    func createMenuOptionButton(title: String) -> UIButton {
        let optionButton: UIButton = {
            let button  = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.contentHorizontalAlignment = UIControl.ContentHorizontalAlignment.left
            button.tintColor = UIColor.white
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        
        optionButton.widthAnchor.constraint(equalToConstant: 80).isActive = true
        optionButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        return optionButton
    }
    
    
    func createBorderGreen(title: String, autoSize: Bool = true) -> UIButton {
        let borderButton: UIButton = {
            let button = UIButton(type: .system)
            button.setTitle(title, for: .normal)
            button.titleLabel?.font = UIFont(name: "Arial Bold", size: 17)
            button.setTitleColor(UIColor.black, for: .normal)
            button.isUserInteractionEnabled = false
            button.backgroundColor = .white
            button.layer.borderWidth = 1
            button.layer.borderColor = UIColor(red: 94/255, green: 186/255, blue: 168/255, alpha: 1).cgColor
            button.translatesAutoresizingMaskIntoConstraints = false
            return button
        }()
        
        if autoSize {
            if title.count < 10 {
                let width: CGFloat = 100
                borderButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
                borderButton.widthAnchor.constraint(equalToConstant: width).isActive = true
            } else {
                let width = CGFloat(title.count + 120)
                borderButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
                borderButton.widthAnchor.constraint(equalToConstant: width).isActive = true
            }
        } else {
            borderButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
        }
        return borderButton
    }
    
    
    func createCategoryButton(imageName: String) -> UIButton {
        let categoryButton: UIButton = {
            let button  = UIButton(type: .custom)
            button.setImage(UIImage(named: imageName), for: .normal)
            button.layer.cornerRadius = 20
            button.layer.masksToBounds = true
            button.backgroundColor = UIColor.init(displayP3Red: 220/255, green: 220/255, blue: 220/255, alpha: 1)
            button.tintColor = UIColor.init(displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            button.translatesAutoresizingMaskIntoConstraints = false
            button.imageView?.translatesAutoresizingMaskIntoConstraints = false
            button.imageView?.contentMode = .scaleAspectFit
            return button
        }()
        
        categoryButton.imageView?.heightAnchor.constraint(equalTo: categoryButton.heightAnchor, multiplier: 0.7).isActive = true
        categoryButton.imageView?.widthAnchor.constraint(equalTo: categoryButton.widthAnchor, multiplier: 0.9).isActive = true
        categoryButton.imageView?.centerYAnchor.constraint(equalTo: categoryButton.centerYAnchor).isActive = true
        
        return categoryButton
    }
    
    func createNewButton(title: String, autoSize: Bool = true) -> UIButton {
            let newButton: UIButton = {
                let button = UIButton(type: .system)
                button.setTitle(title, for: .normal)
                button.titleLabel?.font = UIFont(name: "Arial Bold", size: 17)
                button.contentHorizontalAlignment = .left 
                button.setTitleColor(UIColor.black, for: .normal)
                button.layer.cornerRadius = 7
                button.backgroundColor = UIColor.init(displayP3Red: 223/255, green: 213/255, blue: 214/255, alpha: 1)
                button.tintColor = UIColor.init(displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
                button.translatesAutoresizingMaskIntoConstraints = false
                return button
            }()
            return newButton
        }
}

