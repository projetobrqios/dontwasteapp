//
//  CreateView.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

class PreStablishedView {
    
    func createDefaultView(border: Bool) -> UIView {
        let simpleView: UIView =  {
            let view = UIView()
            view.translatesAutoresizingMaskIntoConstraints = false
            
            if border {
                view.layer.borderWidth = 1
                view.layer.borderColor = CGColor.init(srgbRed: 94/255, green: 186/255, blue: 168/255, alpha: 1)
            }
            
            return view
        }()
        return simpleView
    }
}
