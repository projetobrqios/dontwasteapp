//
//  CreateImages.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 26/08/21.
//

import UIKit

class Image {
    
    func createDefaultImage(imageName: String, alpha: CGFloat = 1, contentMode: UIView.ContentMode) -> UIImageView {
        let background: UIImageView = {
            let imageView = UIImageView()
            imageView.image = UIImage(named: imageName)
            imageView.contentMode = contentMode
            imageView.alpha = alpha
            imageView.translatesAutoresizingMaskIntoConstraints = false
            return imageView
        }()
        return background
    }
}
