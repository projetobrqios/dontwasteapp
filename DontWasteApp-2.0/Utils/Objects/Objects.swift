//
//  CreateObjects.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 27/08/21.
//

import UIKit

class Objects {
    
    func createScrollView() -> UIScrollView {
        let scrollView: UIScrollView = {
           let view = UIScrollView()
           view.isUserInteractionEnabled = true
           view.translatesAutoresizingMaskIntoConstraints = false
           return view
       }()
       return scrollView
    }
    
    func createStackView() -> UIStackView {
        let stackView: UIStackView = {
            let view = UIStackView()
            view.axis = .vertical
            view.isUserInteractionEnabled = true
            view.spacing = 0
            view.translatesAutoresizingMaskIntoConstraints = false
            return view
        }()
        return stackView
    }
    
    func createPickerView() -> UIPickerView {
        let pickerView: UIPickerView = {
            let picker = UIPickerView()
            return picker
        }()
        return pickerView
    }
    
    func createDatePickerView() -> UIDatePicker {
        let datePickerView: UIDatePicker = {
            let datePicker = UIDatePicker()
            datePicker.datePickerMode = .date
            datePicker.datePickerMode = UIDatePicker.Mode.date
            datePicker.preferredDatePickerStyle = .wheels
            let local = Locale(identifier: "pt-br")
            datePicker.locale = local
            
            // Configurando para exibir apenas o último ano
            let currentDate: Date = Date()
            var callendar: Calendar = Calendar(identifier: Calendar.Identifier.gregorian)
            callendar.timeZone = TimeZone(identifier: "UTC")!
            
            var components: DateComponents = DateComponents()
            components.calendar = callendar
            components.year = 0
            let maximumDate: Date = callendar.date(byAdding: components, to: currentDate)!
            components.year = -1
            let minimumDate: Date = callendar.date(byAdding: components, to: currentDate)!
            datePicker.minimumDate = minimumDate
            datePicker.maximumDate = maximumDate
            
            return datePicker
        }()
        
        return datePickerView
    }
    
    
}
