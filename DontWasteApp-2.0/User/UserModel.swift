//
//  UserModel.swift
//  DontWasteApp-2.0
//
//  Created by user on 25/08/21.
//

import Foundation

class User: CustomStringConvertible {
    
    //MARK: - Atributes
    private var email: String
    private var password: String
    private var name: String
    private var howManyRoommates: String
    private var howOftenShop: String
    private var dateLastPurchase: String
    
    //MARK: - CustomStringConvertible Protocol
    var description: String {
        var description = ">>>>> USER INFO <<<<<\n"
        description += ". email: \(getEmail())\n"
        description += ". password: \(getPassword())\n"
        description += ". nome: \(getName())\n"
        description += ". quantidade de pessoas na casa: \(getHowManyRoommates())\n"
        description += ". frequência da compra: \(getHowOftenShop())\n"
        description += ". data da última compra: \(getDateLastPurchase())\n"
        
        return description
    }
    
    init() {
        self.email = "email?"
        self.password = "password?"
        self.name = "name?"
        self.howManyRoommates = "howManyRoommates?"
        self.howOftenShop = "howOftenShop?"
        self.dateLastPurchase = "dateLastPurchase?"
    }
    
    //MARK: - Methods
    func setLogin(_ email: String, _ password: String) {
        self.email = email
        self.password = password
    }
    func setInfo(_ name: String, _ howManyRoommates: String, _ howOftenShop: String, _ dateLastPurchase: String) {
        self.name = name
        self.howManyRoommates = howManyRoommates
        self.howOftenShop = howOftenShop
        self.dateLastPurchase = dateLastPurchase
    }
    func getEmail() -> String {
        return self.email
    }
    func getPassword() -> String {
        return self.password
    }
    func getName() -> String {
        return self.name
    }
    func getHowManyRoommates() -> String {
        return self.howManyRoommates
    }
    func getHowOftenShop() -> String {
        return self.howOftenShop
    }
    func getDateLastPurchase() -> String {
        return self.dateLastPurchase
    }
}
