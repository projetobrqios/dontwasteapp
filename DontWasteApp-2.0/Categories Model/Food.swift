//
//  Food.swift
//  DontWasteApp-2.0
//
//  Created by user on 04/11/21.
//

class Food
{
    // declaracao das Propriedades
    // categoria, nome do alimento , quantidade.
    
    var cathegory: String = ""
    var nameFood: String = ""
    var amount: Int = 0
    
    //declaracao dos metodos(funcoes)
    func setFood(at cathegory: String = "GELADEIRA",_ name: String, _ amount: Int = 0)
    {
        self.cathegory = cathegory
        self.nameFood = name
        self.amount = amount
    }
    
    func expiredFood() {
        print("Alimento Vencido")
    }
}

