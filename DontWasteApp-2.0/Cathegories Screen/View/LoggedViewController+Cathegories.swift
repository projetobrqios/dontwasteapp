//
//  LoggedViewController+Cathegories.swift
//  DontWasteApp-2.0
//
//  Created by user on 08/11/21.
//

import UIKit

extension LoggedViewController
{
    func setCathegoryButtons()
    {
        // MARK: - ATRIBUTES
        let refrigeratorLabel = TextLabel1().createDefaultLabel(
            text: "GELADEIRA",
            size: 17,
            bold: true
        )

        let pantryLabel = TextLabel1().createDefaultLabel(
            text: "DESPENSA",
            size: 17,
            bold: true
        )
        view.addSubview(refrigeratorButton)
        view.addSubview(refrigeratorLabel)
        
        view.addSubview(pantryButton)
        view.addSubview(pantryLabel)
        
        //constraints
        NSLayoutConstraint.activate(
            [
                refrigeratorLabel.topAnchor.constraint(
                        equalTo: refrigeratorButton.topAnchor, constant: 23.5
                    ),
                refrigeratorLabel.centerXAnchor.constraint(
                    equalTo: refrigeratorButton.centerXAnchor
                ),
                
                refrigeratorButton.topAnchor.constraint(
                    equalTo: boxBorder.bottomAnchor, constant: spaceHeight
                ),
                refrigeratorButton.leadingAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 5
                ),
                refrigeratorButton.widthAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.482
                ),
                refrigeratorButton.heightAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.40
                ),
                
                pantryLabel.topAnchor.constraint(
                    equalTo: pantryButton.topAnchor, constant: 23.5
                ),
                pantryLabel.centerXAnchor.constraint(
                    equalTo: pantryButton.centerXAnchor
                ),
                
                pantryButton.topAnchor.constraint(
                    equalTo: boxBorder.bottomAnchor, constant: spaceHeight
                ),
                pantryButton.trailingAnchor
                    .constraint(
                        equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -5
                    ),
                pantryButton.widthAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.482
                ),
                pantryButton.heightAnchor.constraint(
                    equalTo: self.view.safeAreaLayoutGuide.heightAnchor, multiplier: 0.40
                )
            ]
        )
    }
}
