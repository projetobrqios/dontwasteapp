//
//  CategoriesScreenView.swift
//  DontWasteApp-2.0
//
//  Created by user on 13/10/21.
//

import UIKit

import UIKit
import FirebaseAuth

class CathegoriesViewController: LoggedViewController
{
    //MARK: - METHODS
    
    //MARK: - view life cycle
    override func viewDidLoad()
    {
        // view
        super.viewDidLoad()
        super.setNavigationBar(true)
        super.setToolBar(true, withLeftButton: true)
        super.setViewWithBanner()
        super.setCathegoryButtons()

        //Button targets
        super.refrigeratorButton.addTarget(
            self,
           action: #selector(goRefrigerator),
            for: .touchUpInside
        )
        super.pantryButton.addTarget(
            self,
            action: #selector(goPantry),
            for: .touchUpInside
        )
   }
    //MARK: - interface actions
    @objc private func goRefrigerator()
    {
        let refrigeratorVC = RefrigeratorViewController()
        navigationController?
            .pushViewController(refrigeratorVC, animated: true)
    }
    @objc private func goPantry()
    {
        let pantryVC = BlankLoggedViewController()
        navigationController?
            .pushViewController(pantryVC, animated: true)
    }
}
