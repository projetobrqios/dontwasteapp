//
//  ViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 17/08/21.
//

import UIKit
import FirebaseAuth

class Registration1ViewController: BasicViewController {
    //MARK: - ATRIBUTES
    private let registration1View = Registration1View()
    private var user = User()
    private var inputEmail = ""
    private var inputPassword = ""
    private let nextButton = Button()
        .createBottomButton(title: "PRÓXIMO", green: true)
    
    //MARK: - METHODS
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(
            true, withRightButton: true
        )
        
        self.view = registration1View
        registration1View.setView()
        
        //Button targets
        registration1View.hintButton.addTarget(
            self,
            action: #selector(showHint),
            for: .touchUpInside
        )
        let tap = UITapGestureRecognizer(
            target: self,
            action: #selector(UIInputViewController.dismissKeyboard)
        )
        view.addGestureRecognizer(tap)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if FirebaseAuth.Auth.auth().currentUser == nil {
            registration1View.emailField
                .becomeFirstResponder()
        }
    }
    //MARK: - interface actions
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc private func showHint() {
        if registration1View.hintButton.isTouchInside {
            registration1View.hintLabel.isHidden = false
        } else {
            registration1View.hintLabel.isHidden = true
        }
        registration1View.timer = Timer.scheduledTimer(
            timeInterval: 3.0,
            target: self,
            selector: #selector(changeLabelHint),
            userInfo: nil,
            repeats: false
        )
    }
    @objc private func changeLabelHint() {
        registration1View.hintLabel.isHidden = true
    }
    func setUser(){
        self.user.setLogin(inputEmail, inputPassword)
        print(self.user)
    }
    //MARK: - toolbar button
    override func addRightButton() -> UIBarButtonItem {
        //item
        let rightItem = UIBarButtonItem(customView: nextButton)
        //button target
        nextButton.addTarget(
            self,
            action: #selector(goRegistration2),
            for: .touchUpInside
        )
        return rightItem
    }
    //MARK: - firebase
    @objc private func goRegistration2() {
        let validation = RegisterValidation()
        
        guard let email = registration1View.emailField.text,
              let password = registration1View.passwordField.text
        else { return }
        if validation.validateEmail(email: email) {
            if validation.validatePassword(password: password) {
                Auth.auth()
                    .signIn(withEmail: email, password: password){
                    user, erro in
                    if user == nil {
                        let returnError = erro! as NSError
                        if let firebaseError = returnError
                            .userInfo[
                                "FIRAuthErrorUserInfoNameKey"
                            ]
                        {
                            let errorCode = firebaseError as! String
                            if errorCode == "ERROR_WRONG_PASSWORD" {
                                Alert(controller: self)
                                    .setAlert(
                                    title: self.registration1View.messages
                                        .alertFirebaseEmailAlreadyExistsTitle,
                                    message: self.registration1View.messages
                                        .alertFirebaseEmailAlreadyExistsContent
                                )
                                do {
                                    try FirebaseAuth.Auth.auth().signOut()
                                }
                                catch {
                                    print(">>>>>>>>>>>>>ERRO: \(error)")
                                }
                            }
                            else {
                                self.inputEmail = email
                                self.inputPassword = password
                                self.setUser()

                                let registration2VC = Registration2ViewController()
                                registration2VC.user = self.user
                                self.navigationController?
                                    .pushViewController(
                                        registration2VC,
                                        animated: true
                                    )
                            }
                        }
                    }
                    else {
                        Alert(controller: self)
                            .setAlert(
                            title: self.registration1View.messages
                                .alertFirebaseEmailAlreadyExistsTitle,
                            message: self.registration1View.messages
                                .alertFirebaseEmailAlreadyExistsContent
                        )
                        do {
                            try FirebaseAuth.Auth.auth().signOut()
                        }
                        catch {
                            print(">>>>>>>>>>>>>>>ERRO: \(error)")
                        }
                    }
                }
            }
            else {
                Alert(controller: self)
                    .setAlert(
                    title: self.registration1View.messages
                        .alertPasswordErrorTitle,
                    message: self.registration1View.messages
                        .alertPasswordErrorContent
                )
            }
        }
        else {
            Alert(controller: self).setAlert(
                title: self.registration1View.messages
                    .alertEmailErrorTitle,
                message: self.registration1View.messages
                    .alertEmailErrorContent
            )
        }
    }
}
