//
//  Registration1View+Constraints.swift
//  DontWasteApp-2.0
//
//  Created by user on 31/08/21.
//

import UIKit

extension Registration1View {
    func setConstraints() {
        let margins = self.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            userImage.centerXAnchor
                .constraint(equalTo: margins.centerXAnchor),
            userImage.topAnchor
                .constraint(equalTo: margins.topAnchor, constant: 45),
            userImage.widthAnchor
                .constraint(equalToConstant: 65),
            userImage.heightAnchor
                .constraint(equalToConstant: 70),
            
            emailField.topAnchor
                .constraint(equalTo: userImage.bottomAnchor, constant: 35),
            emailField.leadingAnchor
                .constraint(equalTo: margins.leadingAnchor),
            emailField.trailingAnchor
                .constraint(equalTo: margins.trailingAnchor),
            
            passwordField.topAnchor
                .constraint(equalTo: emailField.bottomAnchor, constant: 15),
            passwordField.leadingAnchor
                .constraint(equalTo: margins.leadingAnchor),
            passwordField.trailingAnchor
                .constraint(equalTo: margins.trailingAnchor),
            
            hintButton.topAnchor
                .constraint(equalTo: passwordField.bottomAnchor, constant: 5),
            hintButton.trailingAnchor
                .constraint(equalTo: margins.trailingAnchor),
            hintButton.heightAnchor.constraint(equalToConstant: 25),
            hintButton.widthAnchor
                .constraint(equalToConstant: 30),
            
            hintLabel.topAnchor
                .constraint(equalTo: hintButton.bottomAnchor, constant: 5),
            hintLabel.trailingAnchor
                .constraint(equalTo: margins.trailingAnchor)
        ])
    }
}
