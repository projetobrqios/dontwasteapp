//
//  Registration1self.swift
//  DontWasteApp-2.0
//
//  Created by user on 31/08/21.
//

import UIKit

class Registration1View: UIView {
    
    //MARK: - ATRIBUTES
    var timer = Timer()
    let messages = Messages()
    
    let userImage = Image()
        .createDefaultImage(
            imageName: "cadastrousuario",
            alpha: 1,
            contentMode: .scaleAspectFit
        )
    
    let emailField = TextField()
        .createDefaultTextField(
            placeholder: "E-mail",
            border: true,
            isPassword: false,
            autoCaps: false,
            leftSpace: true
    )
    let passwordField = TextField()
        .createDefaultTextField(
            placeholder: "Senha",
            border: true,
            isPassword: true,
            autoCaps: false,
            leftSpace: true
    )
    let hintButton = Button()
        .createImageButton(imageName: "cadastro_1_icone_ajuda")
    let hintLabel = TextLabel1().createDefaultLabel(
        text: "A senha deve conter de 6 à 8 caracteres",
        size: 15,
        bold: true
    )
    
    //MARK: - METHODS
    func setView(){
        self.backgroundColor = .white
        
        hintLabel.isHidden = true
        
        emailField.keyboardType = .emailAddress
        passwordField.keyboardType = .numberPad
        
        self.addSubview(userImage)
        self.addSubview(emailField)
        self.addSubview(passwordField)
        self.addSubview(hintButton)
        self.addSubview(hintLabel)
        
        setConstraints()
    }
}
