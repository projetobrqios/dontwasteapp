//
//  Teste.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 27/08/21.
//

import UIKit
import FirebaseAuth

class Registration2ViewController: BasicViewController {
    
    //MARK: - ATRIBUTES
    private let registration1VC = Registration1ViewController()
    let registration2View = Registration2View()
    var user = User()
    var name = ""
    var howManyRoommates = ""
    var howOftenShop = ""
    var dateLastPurchase = ""
    private let saveButton = Button()
        .createBottomButton(title: "SALVAR", green: true)

    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(
            true, withLeftButton: true, withRightButton: true
        )
        
        self.view = registration2View
        
        registration2View.amountPickerView.delegate = self
        registration2View.amountPickerView.dataSource = self
        
        registration2View.frequencyPickerView.delegate = self
        registration2View.frequencyPickerView.dataSource = self

        registration2View.setView()
        
        //Button targets
        registration2View.datePickerView.addTarget(
            self,
            action: #selector(didSelectDate(_:)),
            for: .valueChanged
        )
        let tap = UITapGestureRecognizer(
            target: view,
            action: #selector(UIView.endEditing)
        )
        view.addGestureRecognizer(tap)
    }
    //MARK: - interface actions
    @objc private func endEditing(){
        view.endEditing(true)
    }
    @objc func didSelectDate(_ sender: UIDatePicker) {
        let dateFormatter: DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        let selectedDate: String = dateFormatter
            .string(from: sender.date)
       
        registration2View.dateTextField.text = selectedDate
        registration2View.dateLastPurchase = selectedDate
    }
    @objc func backButtonLink(sender: UIButton!) {
        navigationController?
            .popViewController(animated: true)
    }
    //MARK: - toolbar button
    override func addRightButton() -> UIBarButtonItem {
        //item
        let rightItem = UIBarButtonItem(customView: saveButton)
        //button target
        saveButton.addTarget(
            self,
            action: #selector(submitSubscription),
            for: .touchUpInside
        )
        return rightItem
    }
    
    func setUser(
        _ name: String,
        _ howManyRoommates: String,
        _ howOftenShop: String,
        _ dateLastPurchase: String
    ){
        user.setInfo(
            name,
            howManyRoommates,
            howOftenShop,
            dateLastPurchase
        )
        print(user)
    }
}
