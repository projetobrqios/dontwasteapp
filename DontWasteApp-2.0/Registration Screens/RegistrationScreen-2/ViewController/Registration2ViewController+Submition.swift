//
//  Registration2ViewController+Submit.swift
//  DontWasteApp-2.0
//
//  Created by user on 01/09/21.
//

import UIKit

extension Registration2ViewController {

    @objc func submitSubscription() {
        let validation = RegisterValidation()
        let messages = Messages.init()
        
        registration2View.name = registration2View.nameTextField.text ?? ""

        if validation.validateName(name: registration2View.name) { // Valida o campo 'Nome'
            
            if validation.validateEmptyField(field: registration2View.amountPeople) { // Valida Quantidade de Pessoas
                if validation.validateEmptyField(
                    field: registration2View.dateLastPurchase
                ) { // Valida Data da Compra
                    if validation.validateEmptyField(
                        field: registration2View.frequencyShopping
                    ) {
                        self.name = registration2View.name
                        self.howManyRoommates = registration2View
                            .amountPeople
                        self.howOftenShop = registration2View
                            .frequencyShopping
                        self.dateLastPurchase = registration2View
                            .dateLastPurchase
                        
                        self.setUser(
                            self.name,
                            self.howManyRoommates,
                            self.howOftenShop,
                            self.dateLastPurchase
                        )
                        
                        // SPRINT 5 / THIAGO //////////////////////////////////////////////////////////////////////////////////////

                        RegistrationManager().setFirebaseUser(email: self.user.getEmail(),
                                                               senha: self.user.getPassword(),
                                                               name: self.user.getName(),
                                                               howManyRoommates: self.user.getHowManyRoommates(),
                                                               dateLastPurchase: self.user.getDateLastPurchase(),
                                                               howOftenShop: self.user.getHowOftenShop())
                        
                        let loggedHomeVC = LoggedHomeViewController()
                        self.navigationController?.pushViewController(loggedHomeVC, animated: true)
                        
                    }
                    else {
                        Alert.init(controller: self)
                            .setAlert(
                            title: messages
                                .alertHowOftenShopErrorTitle,
                            message: messages
                                .alertHowOftenShopErrorContent
                        )
                    }
                }
                else {
                    Alert.init(controller: self)
                        .setAlert(
                        title: messages
                            .alertDateErrorTitle,
                        message: messages
                            .alertDateErrorContent
                    )
                }
            }
            else {
                Alert.init(controller: self)
                    .setAlert(
                    title: messages
                        .alertHowManyRoomatesErrorTitle,
                    message: messages
                        .alertHowManyRoomatesErrorContent
                )
            }
        }
        else {
            
            Alert.init(controller: self)
                .setAlert(
                title: messages
                    .alertNameErrorTitle,
                message: messages
                    .alertNameErrorContent
            )
        }
    }
}
