//
//  Cadastro2+PickerView.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 27/08/21.
//

import UIKit

extension Registration2ViewController: UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 1
    }
    func pickerView(
        _ pickerView: UIPickerView,
        numberOfRowsInComponent component: Int
    ) -> Int {
        
        if pickerView == registration2View.amountPickerView {
            
            return registration2View.amountPeopleOptions.count
            
        }
        else if pickerView == registration2View.frequencyPickerView {
            
            return registration2View.frequencyShoppingOptions.count
        }
        else {
            
            return 0
        }
    }
    func pickerView(
        _ pickerView: UIPickerView,
        titleForRow row: Int,
        forComponent component: Int
    ) -> String? {
        
        if pickerView == registration2View.amountPickerView {
            
            let row = registration2View.amountPeopleOptions[row]
            return row
        }
        else if pickerView == registration2View.frequencyPickerView {
            
            let row = registration2View.frequencyShoppingOptions[row]
            return row
        }
        else {
            
            return nil
        }
    }
    func pickerView(
        _ pickerView: UIPickerView,
        didSelectRow row: Int,
        inComponent component: Int
    ) {
        
        if pickerView == registration2View.amountPickerView {
            
            registration2View.amountTextField.text = registration2View.amountPeopleOptions[row]
            registration2View.amountPeople = registration2View.amountPeopleOptions[row]
        }
        else if pickerView == registration2View.frequencyPickerView {
            
            registration2View.frequencyTextField.text = registration2View.frequencyShoppingOptions[row]
            registration2View.frequencyShopping = registration2View.frequencyShoppingOptions[row]
        }
    }
}
