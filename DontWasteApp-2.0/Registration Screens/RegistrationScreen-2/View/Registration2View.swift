//
//  CadastroView.swift
//  DontWastAppPaThi
//
//  Created by user on 19/08/21.
//

import UIKit

class Registration2View: UIView
{
    //MARK: - ATRIBUTES
    let scrollView = Objects().createScrollView()
    let stackView = Objects().createStackView()
    
    let emptyContainer = PreStablishedView()
        .createDefaultView(border: false)
    
    let userImageContainer = PreStablishedView()
        .createDefaultView(border: false)
    let userImage = Image()
        .createDefaultImage(
            imageName: "cadastrousuario",
            alpha: 1,
            contentMode: .scaleAspectFit
        )

    let nameContainer = PreStablishedView()
        .createDefaultView(border: false)
    let nameBorder = PreStablishedView()
        .createDefaultView(border: true)
    let nameTextLabel = TextLabel1()
        .createDefaultLabel(text: "Nome:", bold: true)
    var nameTextField = TextField()
        .createDefaultTextField(
            placeholder: "Seu nome completo",
            leftSpace: false
        )
    var name = ""
    
    let amountContainer = PreStablishedView()
        .createDefaultView(border: false)
    let amountBorder = PreStablishedView()
        .createDefaultView(border: true)
    let amountTextLabel = TextLabel1()
        .createDefaultLabel(
        text: "Quantidade de Pessoas que residem na casa:",
        bold: true
    )
    var amountTextField = TextField()
        .createDefaultTextField(
            placeholder: "Selecione",
            leftSpace: false
        )
    let amountPickerView = Objects().createPickerView()
    var amountPeople = ""
    let amountPeopleOptions = [ "1", "2", "3 ou Mais" ]
    
    let dateContainer = PreStablishedView()
        .createDefaultView(border: false)
    let dateBorder = PreStablishedView()
        .createDefaultView(border: true)
    let dateTextLabel = TextLabel1()
        .createDefaultLabel(
            text: "Qual a data da sua última compra?",
            bold: true
        )
    var dateTextField = TextField()
        .createDefaultTextField(
            placeholder: "Selecione",
            leftSpace: false
        )
    let datePickerView = Objects().createDatePickerView()
    var dateLastPurchase = ""

    let frequencyContainer = PreStablishedView()
        .createDefaultView(border: false)
    let frequencyBorder = PreStablishedView()
        .createDefaultView(border: true)
    let frequencyTextLabel = TextLabel1()
        .createDefaultLabel(
            text: "Com que frequência faz compras?" ,
            bold: true
        )
    var frequencyTextField = TextField()
        .createDefaultTextField(
            placeholder: "Selecione",
            leftSpace: false
        )
    let frequencyPickerView = Objects().createPickerView()
    var frequencyShopping = ""
    let frequencyShoppingOptions = [
        "Pontual", "Semanal", "Quinzenal", "Mensal"
    ]
    
    //MARK: - METHODS
    func setView()
    {
        self.backgroundColor = .white
        
        // ADD SUBVIEW
        self.addSubview(scrollView)
        scrollView.addSubview(stackView)
        
        stackView.addArrangedSubview(emptyContainer)
        
        stackView.addArrangedSubview(userImageContainer)
        userImageContainer.addSubview(userImage)
        
        stackView.addArrangedSubview(nameContainer)
        nameContainer.addSubview(nameBorder)
        nameContainer.addSubview(nameTextLabel)
        nameContainer.addSubview(nameTextField)
        
        stackView.addArrangedSubview(amountContainer)
        amountContainer.addSubview(amountBorder)
        amountContainer.addSubview(amountTextLabel)
        amountContainer.addSubview(amountTextField)
        
        stackView.addArrangedSubview(dateContainer)
        dateContainer.addSubview(dateBorder)
        dateContainer.addSubview(dateTextLabel)
        dateContainer.addSubview(dateTextField)
        
        stackView.addArrangedSubview(frequencyContainer)
        frequencyContainer.addSubview(frequencyBorder)
        frequencyContainer.addSubview(frequencyTextLabel)
        frequencyContainer.addSubview(frequencyTextField)
        
        // INPUT
        amountTextField.inputView = amountPickerView
        frequencyTextField.inputView = frequencyPickerView
        dateTextField.inputView = datePickerView
        
        setupConstraints()
    }
}
