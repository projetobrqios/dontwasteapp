//
//  Registration2View+Constraints.swift
//  DontWasteApp-2.0
//
//  Created by user on 31/08/21.
//

import UIKit

extension Registration2View
{

    func setupConstraints()
    {
        
        NSLayoutConstraint.activate([
            scrollView.leadingAnchor.constraint(equalTo: self.layoutMarginsGuide.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: self.layoutMarginsGuide.trailingAnchor),
            scrollView.topAnchor.constraint(equalTo: self.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor),

            stackView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor),
            stackView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            stackView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            stackView.widthAnchor.constraint(equalTo: scrollView.widthAnchor),
            
            emptyContainer.topAnchor.constraint(equalTo: stackView.topAnchor),
            emptyContainer.heightAnchor.constraint(equalToConstant: 40),
            
            userImageContainer.topAnchor.constraint(equalTo: emptyContainer.bottomAnchor),
            userImageContainer.bottomAnchor.constraint(equalTo: userImage.bottomAnchor, constant: 10),
            
            userImage.centerXAnchor.constraint(equalTo: userImageContainer.centerXAnchor),
            userImage.topAnchor.constraint(equalTo: emptyContainer.bottomAnchor, constant: 35),
            userImage.widthAnchor.constraint(equalToConstant: 65),
            userImage.heightAnchor.constraint(equalToConstant: 70),

            nameContainer.topAnchor.constraint(equalTo: userImageContainer.bottomAnchor),
            nameContainer.bottomAnchor.constraint(equalTo: nameTextField.bottomAnchor, constant: 10),
            
            nameBorder.topAnchor.constraint(equalTo: nameContainer.topAnchor, constant: 50),
            nameBorder.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            nameBorder.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            nameBorder.bottomAnchor.constraint(equalTo: nameContainer.bottomAnchor),

            nameTextLabel.topAnchor.constraint(equalTo: nameBorder.topAnchor, constant: 15),
            nameTextLabel.leadingAnchor.constraint(equalTo: nameBorder.leadingAnchor, constant: 15),
            nameTextLabel.trailingAnchor.constraint(equalTo: nameBorder.trailingAnchor, constant: -15),

            nameTextField.topAnchor.constraint(equalTo: nameTextLabel.bottomAnchor, constant: 5),
            nameTextField.leadingAnchor.constraint(equalTo: nameBorder.leadingAnchor, constant: 15),
            nameTextField.trailingAnchor.constraint(equalTo:  nameBorder.trailingAnchor, constant: -15),
            
            amountContainer.topAnchor.constraint(equalTo: nameContainer.bottomAnchor),
            amountContainer.bottomAnchor.constraint(equalTo: amountTextField.bottomAnchor, constant: 10),

            amountBorder.topAnchor.constraint(equalTo: amountContainer.topAnchor, constant: 20),
            amountBorder.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            amountBorder.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            amountBorder.bottomAnchor.constraint(equalTo: amountContainer.bottomAnchor),

            amountTextLabel.topAnchor.constraint(equalTo: amountBorder.topAnchor, constant: 15),
            amountTextLabel.leadingAnchor.constraint(equalTo: amountBorder.leadingAnchor, constant: 15),
            amountTextLabel.trailingAnchor.constraint(equalTo: amountBorder.trailingAnchor, constant: -15),

            amountTextField.topAnchor.constraint(equalTo: amountTextLabel.bottomAnchor, constant: 5),
            amountTextField.leadingAnchor.constraint(equalTo: amountBorder.leadingAnchor, constant: 15),
            amountTextField.trailingAnchor.constraint(equalTo: amountBorder.trailingAnchor, constant: -15),
            
            dateContainer.topAnchor.constraint(equalTo: amountContainer.bottomAnchor),
            dateContainer.bottomAnchor.constraint(equalTo: dateTextField.bottomAnchor, constant: 10),

            dateBorder.topAnchor.constraint(equalTo: dateContainer.topAnchor, constant: 20),
            dateBorder.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            dateBorder.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            dateBorder.bottomAnchor.constraint(equalTo: dateContainer.bottomAnchor),

            dateTextLabel.topAnchor.constraint(equalTo: dateBorder.topAnchor, constant: 15),
            dateTextLabel.leadingAnchor.constraint(equalTo: dateBorder.leadingAnchor, constant: 15),
            dateTextLabel.trailingAnchor.constraint(equalTo: dateBorder.trailingAnchor, constant: -15),

            dateTextField.topAnchor.constraint(equalTo: dateTextLabel.bottomAnchor, constant: 5),
            dateTextField.leadingAnchor.constraint(equalTo: dateBorder.leadingAnchor, constant: 15),
            dateTextField.trailingAnchor.constraint(equalTo: dateBorder.trailingAnchor, constant: -15),
            
            frequencyContainer.topAnchor.constraint(equalTo: dateContainer.bottomAnchor),
            frequencyContainer.bottomAnchor.constraint(equalTo: frequencyTextField.bottomAnchor, constant: 10),

            frequencyBorder.topAnchor.constraint(equalTo: frequencyContainer.topAnchor, constant: 20),
            frequencyBorder.leadingAnchor.constraint(equalTo: stackView.leadingAnchor),
            frequencyBorder.trailingAnchor.constraint(equalTo: stackView.trailingAnchor),
            frequencyBorder.bottomAnchor.constraint(equalTo: frequencyContainer.bottomAnchor),
            
            frequencyTextLabel.topAnchor.constraint(equalTo: frequencyBorder.topAnchor, constant: 15),
            frequencyTextLabel.leadingAnchor.constraint(equalTo: frequencyBorder.leadingAnchor, constant: 15),
            frequencyTextLabel.trailingAnchor.constraint(equalTo: frequencyBorder.trailingAnchor, constant: -15),

            frequencyTextField.topAnchor.constraint(equalTo: frequencyTextLabel.bottomAnchor, constant: 5),
            frequencyTextField.leadingAnchor.constraint(equalTo: frequencyBorder.leadingAnchor, constant: 15),
            frequencyTextField.trailingAnchor.constraint(equalTo: frequencyBorder.trailingAnchor, constant: -15)
        ])
    }
}
