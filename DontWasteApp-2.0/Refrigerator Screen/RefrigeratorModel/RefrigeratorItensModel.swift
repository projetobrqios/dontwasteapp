//
//  SampleData.swift
//  CollectionViewProgrammatically
//
//  Created by Cong Le on 8/6/21.
//

import UIKit

struct itensModel {
    
    let imageName: String
    let link: UIViewController
    let description: String
}

