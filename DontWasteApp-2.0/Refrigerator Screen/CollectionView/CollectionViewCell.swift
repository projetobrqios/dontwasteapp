//
//  CollectionViewCell.swift
//  Collection
//
//  Created by Thiago de Angele on 18/10/21.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
  static let reuseIdentifier = "collectionViewCell"
  lazy var imageView = makeImageView()
  lazy var label = makeLabel()
  
  var refrigeratorItens: itensModel? {
    didSet {
      guard let data = refrigeratorItens else { return }
      label.text = data.description
      imageView.image = UIImage(named: data.imageName)
    }
  }
 
  override init(frame: CGRect) {
    super.init(frame: frame)
    contentView.addSubview(imageView)
    contentView.addSubview(label)
    setupConstraints()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

// MARK: - Setup constraints
extension CollectionViewCell {
  func setupConstraints() {
    imageView.translatesAutoresizingMaskIntoConstraints = false
    
    NSLayoutConstraint.activate([
        label.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -15),
        label.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.9),
        label.centerXAnchor.constraint(equalTo: contentView.centerXAnchor),
        
        imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 15),
        imageView.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.9),
        imageView.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.5),
        imageView.centerXAnchor.constraint(equalTo: contentView.centerXAnchor)
    ])
  }
}

// MARK: - Setup UI components
extension CollectionViewCell {
    
    private func makeImageView() -> UIImageView {
        let view = UIImageView()
        view.layer.masksToBounds = true
        view.contentMode = .scaleAspectFit
        return view
    }
    
    private func makeLabel() -> UILabel {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = UIFont(name: "Arial Bold", size: 16)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        return label
    }
}
