//
//  CollectionViewDataModel.swift
//  Collection
//
//  Created by Thiago de Angele on 19/10/21.
//

import UIKit

// MARK: - Setup UI components
extension RefrigeratorViewController {
    
    func makeFlowLayout() -> UICollectionViewFlowLayout {
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumLineSpacing = 2.0
        layout.minimumInteritemSpacing = 2.0
        layout.sectionInset = UIEdgeInsets(
            top: 2.0,
            left: 2.0,
            bottom: 2.0,
            right: 2.0
        )
        return layout
    }
    func makeCollectionView() -> UICollectionView {
        
        let collectionView = UICollectionView(
            frame: .zero,
            collectionViewLayout: collectionFlowLayout
        )
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = .white
        collectionView.register(
            CollectionViewCell.self,
            forCellWithReuseIdentifier: CollectionViewCell.reuseIdentifier
        )
        return collectionView
    }
}

//MARK: - UICollectionViewDelegate
extension RefrigeratorViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
        didSelectItemAt indexPath: IndexPath) {
        
        let controller = refrigeratorItens[indexPath.row].link
        controller.modalPresentationStyle = .fullScreen
        self.navigationController?.pushViewController(controller, animated:true)
    }
}

// MARK: - UICollectionViewDataSource
extension RefrigeratorViewController: UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    func collectionView(
        _ collectionView: UICollectionView,
        numberOfItemsInSection section: Int
    ) -> Int {
        
        return refrigeratorItens.count
    }
    func collectionView(
        _ collectionView: UICollectionView,
        cellForItemAt indexPath: IndexPath
    ) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: CollectionViewCell.reuseIdentifier,
            for: indexPath) as? CollectionViewCell
        else {
            fatalError("Erro ao obter a célula")
        }

        cell.refrigeratorItens = refrigeratorItens[indexPath.row]
        cell.layer.cornerRadius = 20
        cell.backgroundColor = UIColor.init(red: 94/255, green: 186/255, blue: 168/255, alpha: 1)
        
        return cell
    }
}

// MARK: - UICollectionViewDelegateFlowLayout
extension RefrigeratorViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let collectionWidth = collectionView.bounds.width
        let spacingBetweenItems: CGFloat = collectionFlowLayout.minimumInteritemSpacing
            
        let collectionItensPerRow: CGFloat = 3.0
        let availableWidth = collectionWidth - spacingBetweenItems * (collectionItensPerRow + 1)
        let itemDimension = floor(availableWidth / collectionItensPerRow)
            
        return CGSize(width: itemDimension, height: itemDimension*1.25)
    }
}
