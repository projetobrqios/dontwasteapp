//
//  ViewController.swift
//  Collection
//
//  Created by Thiago de Angele on 14/10/21.
//

import UIKit

class RefrigeratorViewController: LoggedViewController
{
    //MARK: - ATRIBUTES
    lazy var collectionFlowLayout = makeFlowLayout()
    lazy var collectionView = makeCollectionView()
    
    //MARK: - METHODS
    
    //MARK: - view life cycle
    override func viewDidLoad()
    {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
        setViewWithBanner(
            withCathegory: true, cathegoryName: "GELADEIRA", cathegoryImageName: "geladeira_aberta"
        )
        
        view.addSubview(collectionView)
        
        setupConstraints()
    }
    
    func setupConstraints()
    {
        NSLayoutConstraint.activate(
            [
                collectionView.topAnchor.constraint(
                    equalTo: internalView.bottomAnchor, constant: spaceHeight
                ),
                collectionView.leadingAnchor.constraint(
                    equalTo: view.safeAreaLayoutGuide.leadingAnchor
                ),
                collectionView.trailingAnchor.constraint(
                    equalTo: view.safeAreaLayoutGuide.trailingAnchor
                ),
                collectionView.bottomAnchor.constraint(
                    equalTo: self.view.bottomAnchor
                )
            ]
        )
    }
}

