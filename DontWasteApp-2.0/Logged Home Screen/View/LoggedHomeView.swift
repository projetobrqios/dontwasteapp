//
//  LoggedHomeView.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/09/21.
//

import UIKit

class LoggedHomeView: UIView {
    
    //MARK: - Atributes
    let container = PreStablishedView()
        .createDefaultView(border: false)
    let space = PreStablishedView()
        .createDefaultView(border: false)
    
    let background = Image()
        .createDefaultImage(
            imageName: "fundo",
            alpha: 0.4,
            contentMode: .scaleAspectFill
    )
    
    let logo = Image()
        .createDefaultImage(
            imageName: "Logo1",
            alpha: 1,
            contentMode: .scaleAspectFit
        )
    
    let titleLabel1 = TextLabel1()
        .createOpenSansLabel(text: "Don't", size: 40, greenColor: false)
    let titleLabel2 = TextLabel1()
        .createOpenSansLabel(text: "Waste", size: 40, greenColor: false)
    let titleLabel3 = TextLabel1()
        .createOpenSansLabel(text: "organize", size: 25, greenColor: false)

    let subtitleLabel1 = TextLabel1()
        .createOpenSansLabel(text: "Sua despensa", size: 40, greenColor: true)
    let subtitleLabel2 = TextLabel1().createOpenSansLabel(
            text: "inteligente",
            size: 40,
            greenColor: true
    )
    
    //MARK: - METHODS
    func setView()  {
        self.backgroundColor = .white
       
        self.addSubview(background)
        self.addSubview(container)
        container.addSubview(logo)
        container.addSubview(titleLabel1)
        container.addSubview(titleLabel2)
        container.addSubview(titleLabel3)
        container.addSubview(space)
        container.addSubview(subtitleLabel1)
        container.addSubview(subtitleLabel2)

        setConstraints()
    }
}

