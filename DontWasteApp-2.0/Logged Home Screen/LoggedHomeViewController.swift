//
//  LoggedHomeScreenVC.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/09/21.
//

import UIKit

class LoggedHomeViewController: LoggedViewController {

    //MARK: - ATRIBUTES
    private let loggedView = LoggedHomeView()
    private var width: CGFloat = 150
    
    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        width = view.frame.width * 0.43
        setNavigationBar(true)
        setToolBar(
            true, withLeftButton: true, withRightButton: true
        )
        
        self.view = loggedView
        loggedView.setView()
   }
    
    //MARK: - toolbar buttons
    override func addLeftButton() -> UIBarButtonItem {
        //item
        let dueDateButton = Button().createBottomButton(
            title: "VENCIMENTOS PRÓX.",
            autoSize: false,
            green: false
        )
        let leftItem = UIBarButtonItem(customView: dueDateButton)
        //constraint
        NSLayoutConstraint.activate([
            dueDateButton.heightAnchor
                .constraint(equalToConstant: 40)
        ])
        leftItem.width = self.width
        //target
        dueDateButton.addTarget(
            self,
           action: #selector(goDueDate),
            for: .touchUpInside
        )
        return leftItem
    }
    @objc private func goDueDate() {
            
        let dueDateVC = DueDateViewController()
        navigationController?.pushViewController(dueDateVC, animated: true)
    }
    override func addRightButton() -> UIBarButtonItem {
        //item
        let wasteButton = Button().createBottomButton(
            title: "DESPERDICÍOS",
            autoSize: false,
            green: true
        )
        let rightItem = UIBarButtonItem(customView: wasteButton)
        //constraint
        NSLayoutConstraint.activate([
            wasteButton.heightAnchor
                .constraint(equalToConstant: 40)
        ])
        rightItem.width = self.width
        //target
        wasteButton.addTarget(
            self,
            action: #selector(goWaste),
            for: .touchUpInside
        )
        return rightItem
    }
    @objc private func goWaste() {
        let goWasteVC = WasteScreenViewController()
        navigationController?
            .pushViewController(goWasteVC, animated: true)
    }
}
