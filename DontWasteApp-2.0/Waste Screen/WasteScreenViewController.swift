//
//  WasteScreenVC.swift
//  DontWasteApp-2.0
//
//  Created by user on 29/09/21.
//

import UIKit

class WasteScreenViewController: LoggedViewController {
    
    //MARK: - ATRIBUTES
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
        setViewWithBanner()
        
        title = "WASTE SCREEN"
        
        self.view.backgroundColor = .white
        
        //Button targets
    }
    
    //MARK: - METHODS
}
