//
//  FirebaseRedirect.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 29/10/21.
//

import UIKit
import FirebaseAuth

class AuthManager: UIViewController {
    
    func firebaseLogin(email: String, password: String, completion: @escaping (Bool) -> Void ) {
        Auth.auth().signIn(withEmail: email, password: password, completion: { (usuario, erro) in
            if erro == nil {
                completion(true)
                return
            } else {
                completion(false)
                return
            }
        })
    }
    
    
    func firebaseLogout() {
        if FirebaseAuth.Auth.auth().currentUser != nil {
            do {
                try FirebaseAuth.Auth.auth().signOut()
            }
            catch {
                print(">>>>>>>>>> Logout Error >>>>>>>>>>")
            }
        }
    }


    func getLoggedUserUid() -> String {
        let auth = Auth.auth()
        guard let idUser = auth.currentUser?.uid else { return "" }
        return idUser
    }
    
     
}
