//
//  DatabaseManager.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 30/10/21.
//

import Foundation
import FirebaseDatabase

class DatabaseManager {
    
    func getUserProfileData(){
        let profile = Database.database().reference().child("users").child(AuthManager().getLoggedUserUid()).child("profile")
        profile.observe(.value) { snapshot in
            
            let dados = snapshot.value as? NSDictionary
            
            let nome = dados?["name"] as! String
            let dateLastPurchase = dados?["dateLastPurchase"] as! String
            let email = dados?["email"] as! String
            let howOftenShop = dados?["howOftenShop"] as! String
            let howManyRoommates = dados?["howManyRoommates"] as! String
            let registrationDate = dados?["registrationDate"]
            
            print("\n\n::::::::::::::::::::::: QUEM LOGOU :::::::::::::::::::::::\n")
            print("Nome: \(nome)")
            print("E-mail: \(email)")
            print("Última Compra: \(dateLastPurchase)")
            print("Frequencia de Compra: \(howOftenShop)")
            print("Moradores em Casa: \(howManyRoommates)")
            print("Data do Cadastro: \(self.convertTimestamp(dataToConvert: registrationDate as! Double))")
            print("\n::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::\n\n\n")
        }
    }
    
    
    func convertTimestamp(dataToConvert: Double) -> String {
        let convert = dataToConvert / 1000
        let date = NSDate(timeIntervalSince1970: convert)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        return dateFormatter.string(from: date as Date)
    }
    
    
}
