//
//  FirebaseAuth.swift
//  DontWasteApp-2.0
//
//  Created by Thiago de Angele on 29/10/21.
//

import UIKit
import FirebaseAuth
import FirebaseDatabase

class RegistrationManager: UIViewController {
    
    func setFirebaseUser(email: String,
                         senha: String,
                         name: String,
                         howManyRoommates: String,
                         dateLastPurchase: String,
                         howOftenShop: String){
        
        Auth.auth().createUser(withEmail: email,password: senha) { user, error in
            if error == nil {
                // Create Profile
                let database = Database.database().reference()
                let users = database.child("users").child(user!.user.uid)
                let timestamp = ServerValue.timestamp()

                let usersData = ["name": name,
                                 "email": email,
                                 "howManyRoommates": howManyRoommates,
                                 "dateLastPurchase": dateLastPurchase,
                                 "howOftenShop": howOftenShop,
                                 "registrationDate": timestamp ] as [String : Any]

            
                users.child("profile").setValue(usersData)
                
                
                
                
                let usuarios = database.child("users/\(AuthManager().getLoggedUserUid())/refrigerator/\(TableRefrigeratorVC().cathegory)")
                let manga = ["foodName": "MANGA", "amount": 1] as [String : Any]
                let mamao = ["foodName": "MAMÃO", "amount": 1] as [String : Any]
                let cenoura = ["foodName": "CENOURA", "amount": 1] as [String : Any]
                let brocolis = ["foodName": "BRÓCOLIS", "amount": 1] as [String : Any]
                
                usuarios.child("MAMÃO").setValue(mamao)
                usuarios.child("MANGA").setValue(manga)
                usuarios.child("CENOURA").setValue(cenoura)
                usuarios.child("BRÓCOLIS").setValue(brocolis)
                
                
            } else {
                let messages = Messages.init()
                Alert(controller: self).setAlert(title: messages.alertFirebaseAuthErrorTitle,
                                                 message: messages.alertFirebaseAuthErrorContent
                )
            }
        }
    }
     
}





