//
//  DueDateViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 22/10/21.
//

import UIKit

class DueDateViewController: LoggedViewController {
    
    //MARK: - ATRIBUTES
    
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
        setViewWithBanner(
            withCathegory: true,
            cathegoryName: "GELADEIRA",
            cathegoryImageName: "geladeira_aberta"
        )
        
        title = "DUE DATE SCREEN"
        
        self.view.backgroundColor = .white
        
        //Button targets
    }
    
    //MARK: - METHODS
}
