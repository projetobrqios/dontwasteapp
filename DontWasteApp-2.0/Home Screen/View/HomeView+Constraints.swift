//
//  HomeView+Constraints.swift
//  DontWasteApp-2.0
//
//  Created by user on 31/08/21.
//

import UIKit

extension HomeView {

    func setConstraints() {
        let margins = self.layoutMarginsGuide
        
        NSLayoutConstraint.activate([
            background.topAnchor
                .constraint(equalTo: self.topAnchor),
            background.bottomAnchor
                .constraint(equalTo: self.bottomAnchor),
            background.leadingAnchor
                .constraint(equalTo: self.leadingAnchor),
            background.trailingAnchor
                .constraint(equalTo: self.trailingAnchor),
            
            container.leadingAnchor
                .constraint(equalTo: margins.leadingAnchor),
            container.trailingAnchor
                .constraint(equalTo: margins.trailingAnchor),
            container.centerYAnchor
                .constraint(equalTo: margins.centerYAnchor),
            container.centerXAnchor
                .constraint(equalTo: margins.centerXAnchor),
            container.heightAnchor
                .constraint(
                    equalTo: self.heightAnchor,
                    multiplier: 0.60
                ),
            
            logo.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            logo.topAnchor
                .constraint(equalTo: container.topAnchor),
            logo.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.30
                ),
            
            titleLabel1.topAnchor
                .constraint(equalTo: logo.bottomAnchor),
            titleLabel1.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            titleLabel1.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.10
                ),
            
            titleLabel2.topAnchor
                .constraint(equalTo: titleLabel1.bottomAnchor),
            titleLabel2.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            titleLabel2.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.10
                ),
            
            titleLabel3.topAnchor
                .constraint(equalTo: titleLabel2.bottomAnchor),
            titleLabel3.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            titleLabel3.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.10
                ),
            
            space.topAnchor
                .constraint(equalTo: titleLabel3.bottomAnchor),
            space.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            space.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.06
                ),
            
            subtitleLabel1.topAnchor
                .constraint(equalTo: space.bottomAnchor),
            subtitleLabel1.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            subtitleLabel1.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.10
                ),
            
            subtitleLabel2.topAnchor
                .constraint(equalTo: subtitleLabel1.bottomAnchor),
            subtitleLabel2.centerXAnchor
                .constraint(equalTo: container.centerXAnchor),
            subtitleLabel2.heightAnchor
                .constraint(
                    equalTo: container.heightAnchor,
                    multiplier: 0.13
                )
        ])
    }
}
