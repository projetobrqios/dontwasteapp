//
//  homeViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 18/08/21.
//

import UIKit
import Firebase

class HomeViewController: BasicViewController,
                          UINavigationControllerDelegate
{
    //MARK: - ATRIBUTES
    private let homeView = HomeView()
    private let firstAcessButton = Button()
        .createBottomButton(
        title: "PRIMEIRO ACESSO", autoSize: false, green: false
    )
    private let loginButton = Button()
        .createBottomButton(
        title: "LOGIN", autoSize: false, green: true
    )
    private var width: CGFloat = 150
    
    //MARK: - METHODS
    //MARK: - View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        width = view.frame.width * 0.43
        super.setNavigationBar(false)
        super.setToolBar(
            true, withLeftButton: true, withRightButton: true
        )
        navigationController?.delegate = self
        
        self.view = homeView
        homeView.setView()
    }
    //MARK: - toolbar buttons
    override func addLeftButton() -> UIBarButtonItem {
        //item
        let leftItem = UIBarButtonItem(customView: firstAcessButton)
        //constraint
        NSLayoutConstraint.activate([
            firstAcessButton.heightAnchor
                .constraint(equalToConstant: 40)
        ])
        leftItem.width = self.width
        //button target
        firstAcessButton.addTarget(
            self,
            action: #selector(register),
            for: .touchUpInside
        )
        return leftItem
    }
    @objc private func register() {
        print("=================first access pressed")
        let registration1VC = Registration1ViewController()
        registration1VC.modalPresentationStyle = .fullScreen
        navigationController?
            .pushViewController(registration1VC, animated: true)
    }
    override func addRightButton() -> UIBarButtonItem {
        //item
        let rightItem = UIBarButtonItem(customView: loginButton)
        //constraint
        NSLayoutConstraint.activate([
            loginButton.heightAnchor
                .constraint(equalToConstant: 40)
        ])
        rightItem.width = self.width
        //button target
        loginButton.addTarget(
            self,
            action: #selector(login),
            for: .touchUpInside
        )
        return rightItem
    }
    @objc private func login() {
        print("=================login pressed")
        let loginVC = LoginViewController()
        navigationController?
            .pushViewController(loginVC, animated: true)
    }
}
