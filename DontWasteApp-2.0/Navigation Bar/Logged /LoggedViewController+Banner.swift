//
//  LoggedViewController+Banner.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit
//MARK: - BANNER
extension LoggedViewController
{
    func setViewWithBanner(
        withCathegory: Bool = false,
        cathegoryName: String = "GELADEIRA",
        cathegoryImageName: String = "geladeira_aberta",
        withSubCathegory: Bool = false,
        subcathegoryName: String = "FRUTAS, LEG. E VERDURAS",
        subcathegoryImageName: String = "frutas"
    ) {
        //view
        view.backgroundColor = .white
        view.addSubview(logo)
        view.addSubview(boxBorder)
        spaceHeight = view.frame.height * 0.01
        //constraints
        let margins = self.view.layoutMarginsGuide
        NSLayoutConstraint.activate(
            [
                logo.topAnchor.constraint(
                    equalTo: margins.topAnchor, constant: spaceHeight
                ),
                logo.heightAnchor.constraint(
                    equalTo: self.view.heightAnchor, multiplier: 0.09
                ),
                logo.centerXAnchor.constraint(
                    equalTo: margins.centerXAnchor
                ),
                
                boxBorder.topAnchor.constraint(
                    equalTo: logo.bottomAnchor, constant: 5
                ),
                boxBorder.centerXAnchor.constraint(
                    equalTo: logo.centerXAnchor
                ),
                boxBorder.heightAnchor.constraint(
                    equalToConstant: 40
                ),
                boxBorder.widthAnchor.constraint(
                    equalTo: margins.widthAnchor, multiplier: 1.12
                )
        ])
        if withCathegory
        {
            //atributes
            let leftView = PreStablishedView().createDefaultView(border: false)
            let rightView = PreStablishedView().createDefaultView(border: false)
            let cathegoryImage = Image().createDefaultImage(
                imageName: cathegoryImageName, contentMode: .scaleAspectFit
            )
            let cathegoryTitle = TextLabel1().createDefaultLabel(
                text: cathegoryName, size: 25, bold: true
            )
            //view
            internalView.backgroundColor = UIColor.init(
                displayP3Red: 220/255, green: 220/255, blue: 220/255, alpha: 1
            )
            internalView.layer.cornerRadius = 20
            view.addSubview(internalView)
            internalView.addSubview(leftView)
            internalView.addSubview(rightView)
            leftView.addSubview(cathegoryImage)
            rightView.addSubview(cathegoryTitle)
            //constraints
            NSLayoutConstraint.activate(
                [
                    internalView.topAnchor.constraint(
                        equalTo: boxBorder.bottomAnchor, constant: spaceHeight
                    ),
                    internalView.widthAnchor.constraint(
                        equalTo: view.safeAreaLayoutGuide.widthAnchor, constant: -5
                    ),
                    internalView.centerXAnchor.constraint(
                        equalTo: view.safeAreaLayoutGuide.centerXAnchor
                    ),
                    internalView.heightAnchor.constraint(
                        equalTo: view.heightAnchor, multiplier: 0.15
                    ),
                    
                    leftView.topAnchor.constraint(
                        equalTo: internalView.topAnchor
                    ),
                    leftView.bottomAnchor.constraint(
                        equalTo: internalView.bottomAnchor
                    ),
                    leftView.leadingAnchor.constraint(
                        equalTo: internalView.leadingAnchor
                    ),
                    leftView.widthAnchor.constraint(
                        equalTo: internalView.widthAnchor, multiplier: 0.35
                    ),
                    
                    cathegoryImage.centerXAnchor.constraint(
                        equalTo: leftView.centerXAnchor
                    ),
                    cathegoryImage.centerYAnchor.constraint(
                        equalTo: leftView.centerYAnchor
                    ),
                    cathegoryImage.heightAnchor.constraint(
                        equalTo: leftView.heightAnchor, multiplier: 0.85
                    ),
                    
                    rightView.topAnchor.constraint(
                        equalTo: internalView.topAnchor
                    ),
                    rightView.bottomAnchor.constraint(
                        equalTo: internalView.bottomAnchor
                    ),
                    rightView.trailingAnchor.constraint(
                        equalTo: internalView.trailingAnchor
                    ),
                    rightView.widthAnchor.constraint(
                        equalTo: internalView.widthAnchor, multiplier: 0.65
                    ),
                    
                    cathegoryTitle.centerXAnchor.constraint(
                        equalTo: rightView.centerXAnchor
                    ),
                    cathegoryTitle.centerYAnchor.constraint(
                        equalTo: rightView.centerYAnchor
                    )
                ]
            )
            if withSubCathegory
            {
                let subLeftView = PreStablishedView().createDefaultView(border: false)
                let subRightView = PreStablishedView().createDefaultView(border: false)
                let subCathegoryImage = Image().createDefaultImage(
                    imageName: subcathegoryImageName, contentMode: .scaleAspectFit
                )
                let subCathegoryTitle = TextLabel1().createDefaultLabel(
                    text: subcathegoryName, size: 14, bold: true
                )
                //view
                subInternalView.backgroundColor = UIColor.init(
                    displayP3Red: 94/255, green: 186/255, blue: 168/255, alpha: 1
                )
                subInternalView.layer.cornerRadius = 10
                view.addSubview(subInternalView)
                subInternalView.addSubview(subLeftView)
                subInternalView.addSubview(subRightView)
                subLeftView.addSubview(subCathegoryImage)
                subRightView.addSubview(subCathegoryTitle)
                //constraints
                NSLayoutConstraint.activate(
                    [
                        subInternalView.topAnchor.constraint(
                            equalTo: internalView.bottomAnchor, constant: spaceHeight/2
                        ),
                        subInternalView.widthAnchor.constraint(
                            equalTo: view.safeAreaLayoutGuide.widthAnchor, constant: -5
                        ),
                        subInternalView.centerXAnchor.constraint(
                            equalTo: view.safeAreaLayoutGuide.centerXAnchor
                        ),
                        subInternalView.heightAnchor.constraint(
                            equalTo: view.heightAnchor, multiplier: 0.08
                        ),
                        
                        subLeftView.topAnchor.constraint(
                            equalTo: subInternalView.topAnchor
                        ),
                        subLeftView.bottomAnchor.constraint(
                            equalTo: subInternalView.bottomAnchor
                        ),
                        subLeftView.leadingAnchor.constraint(
                            equalTo: subInternalView.leadingAnchor, constant: 3
                        ),
                        subLeftView.widthAnchor.constraint(
                            equalTo: subInternalView.widthAnchor, multiplier: 0.3
                        ),
                        
                        subCathegoryImage.centerXAnchor.constraint(
                            equalTo: subLeftView.centerXAnchor
                        ),
                        subCathegoryImage.centerYAnchor.constraint(
                            equalTo: subLeftView.centerYAnchor
                        ),
                        subCathegoryImage.heightAnchor.constraint(
                            equalTo: subLeftView.heightAnchor, multiplier: 0.85
                        ),
                        
                        subRightView.topAnchor.constraint(
                            equalTo: subInternalView.topAnchor
                        ),
                        subRightView.bottomAnchor.constraint(
                            equalTo: subInternalView.bottomAnchor
                        ),
                        subRightView.trailingAnchor.constraint(
                            equalTo: subInternalView.trailingAnchor
                        ),
                        subRightView.widthAnchor.constraint(
                            equalTo: subInternalView.widthAnchor, multiplier: 0.7
                        ),
                        
                        subCathegoryTitle.leadingAnchor.constraint(
                            equalTo: subRightView.leadingAnchor
                        ),
                        subCathegoryTitle.centerYAnchor.constraint(
                            equalTo: subRightView.centerYAnchor
                        )
                    ]
                )
            }
        }
    }
}
