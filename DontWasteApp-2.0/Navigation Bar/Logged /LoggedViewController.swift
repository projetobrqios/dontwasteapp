//
//  LoggedHome.swift
//  DontWasteApp-2.0
//
//  Created by user on 19/10/21.
//

import UIKit
import FirebaseAuth
import GoogleDataTransport

class LoggedViewController: UIViewController {
    
    //MARK: - ATRIBUTES
    let homeButton = Button()
        .createImageButton(imageName: "cadastrohome")
    
    let burgButton = Button()
        .createImageButton(imageName: "burgicon")
    let menuView = MenuView()
    
    let searchButton = Button()
        .createImageButton(imageName: "Lupa")
    let searchBar = UISearchBar()
    
    let logo = Image().createDefaultImage(
        imageName: "logo_for_logged_area",
        contentMode: .scaleAspectFit
    )
    let boxBorder = Button().createBorderGreen(
        title: "Clique para cadastrar o que há na sua:",
        autoSize: false
    )
    
    let internalView = PreStablishedView().createDefaultView(border: false)
    let subInternalView = PreStablishedView().createDefaultView(border: false)
    
    let refrigeratorButton = Button()
        .createCategoryButton(
            imageName: "categorias_geladeira"
        )
    let pantryButton = Button()
        .createCategoryButton(
            imageName: "categorias_despensa"
        )
    
    var spaceHeight: CGFloat = 150
    
    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: - toolbar
    func setToolBar(
        _ withButtons: Bool,
        withLeftButton: Bool = false,
        withRightButton: Bool = false
    ) {
        if withButtons {
            navigationController?.isToolbarHidden = false
            
            
            let spacer = UIBarButtonItem(
                barButtonSystemItem: .flexibleSpace,
                target: nil,
                action: nil
            )
            let leftItem = addLeftButton()
            let rightItem = addRightButton()
            
            if withLeftButton && withRightButton {
                toolbarItems = [ leftItem, spacer , rightItem ]
            }
            else if withLeftButton {
                toolbarItems = [ leftItem, spacer ]
            }
            else {
                toolbarItems = [ spacer, rightItem ]
            }
        }
        else {
            navigationController?.isToolbarHidden = true
        }
    }
    //MARK: - toolbar items
    func addLeftButton() -> UIBarButtonItem {
        let backButton = Button()
            .createBottomButton(title: "VOLTAR", green: false)
        let leftItem = UIBarButtonItem(customView: backButton)
        
        backButton.addTarget(
            self,
            action: #selector(goBack),
            for: .touchUpInside
        )
        return leftItem
    }
    func addRightButton() -> UIBarButtonItem {
        let backButton = Button()
            .createBottomButton(title: "VOLTAR", green: true)
        let rightItem = UIBarButtonItem(customView: backButton)
        
        backButton.addTarget(
            self,
            action: #selector(goBack),
            for: .touchUpInside
        )
        return rightItem
    }
    @objc func goBack(){
        print("=================goBack pressed")
        let navArray: [UIViewController] = self.navigationController!.viewControllers as Array
        if navArray[(navArray.count)-2] == LoggedHomeViewController()
        {
            self.setNavigationBarTransparent()
            self.setToolbarTransparent()
        }
        navigationController?
            .popViewController(animated: true)
    }
    func setToolbarTransparent(){
        navigationController?
            .extendedLayoutIncludesOpaqueBars = false
        if let toolbarSet = navigationController?.toolbar
        {
            toolbarSet.standardAppearance
                .configureWithTransparentBackground()
            toolbarSet.isTranslucent = true
        }
        else {
            print(">>>>>>>>>>TOOLBAR CONFIG não funciona")
        }
    }
}

