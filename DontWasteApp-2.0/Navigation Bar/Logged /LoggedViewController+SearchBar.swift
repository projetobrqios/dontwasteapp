//
//  LoggedViewController+SearchBar.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit
//MARK: - SEARCHBAR
extension LoggedViewController {

    func showSearchBarButton(shouldShow: Bool) {
        if shouldShow {
            setButtonConstraints()
            let homeItem = UIBarButtonItem(customView: homeButton)
            let searchItem = UIBarButtonItem(customView: searchButton)
            let burgItem = UIBarButtonItem(customView: burgButton)
            
            navigationItem.leftBarButtonItems = [ homeItem ]
            navigationItem.rightBarButtonItems = [ burgItem, searchItem ]
            
            homeButton.addTarget(
                self,
                action: #selector(goHomeNV),
                for: .touchUpInside
            )
            searchButton.addTarget(
                self,
                action: #selector(handleShowSearchBar),
                for: .touchUpInside
            )
            burgButton.addTarget(
                self,
                action: #selector(openMenu),
                for: .touchUpInside
            )
        } else {
            navigationItem.rightBarButtonItems = []
            navigationItem.leftBarButtonItems = []
            navigationItem.hidesBackButton = true
            
            searchBar.showsCancelButton = true
        }
    }
    func search(shouldShow:Bool) {
        showSearchBarButton(shouldShow: !shouldShow)
        searchBar.showsCancelButton = shouldShow
        navigationItem.titleView = shouldShow ? searchBar : nil
    }
    @objc func goHomeNV() {
        print("===========goHome @ LoggedHomeVC pressed")
        for controller in self.navigationController!
                .viewControllers as Array {
            if controller.isKind(of: LoggedHomeViewController.self)
            {
                self.setNavigationBarTransparent()
                self.setToolbarTransparent()
                _ =  self.navigationController!
                    .popToViewController(controller, animated: true)
                break
            }
        }
    }
    @objc func handleShowSearchBar() {
        print("===========LUPA pressed")
        search(shouldShow: true)
        searchBar.becomeFirstResponder()
    }
}
//MARK: - SEARCH BAR DELEGATE
extension LoggedViewController: UISearchBarDelegate{
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        print(">>>>>>>>>>>Search bar did begin editing")
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print("Search Text is \(searchText)")
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(">>>>>>>>>>>Search bar did end editing")
    }
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        search(shouldShow: false)
    }
}
