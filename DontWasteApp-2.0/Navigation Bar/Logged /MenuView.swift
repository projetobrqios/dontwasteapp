//
//  MenuView.swift
//  DontWasteApp-2.0
//
//  Created by user on 18/10/21.
//

import UIKit
import FirebaseAuth

class MenuView: UIView {

    //MARK: - ATRIBUTES
    let burgView: UIView = {
        let menuView = UIView()
        menuView.backgroundColor = UIColor(displayP3Red: 0/255, green: 0/255, blue: 0/255, alpha: 0.7)
        menuView.translatesAutoresizingMaskIntoConstraints = false

        return menuView
    }()
    let option1Button = Button().createMenuOptionButton(title: "Cadastro")
    let option2Button = Button().createMenuOptionButton(title: "Sair")
//    let option3Button = Button().createMenuOptionButton(title: "---")
    
    //MARK: - METHODS
    func setView(inside: UIView){
        print(">>>>>>>>>>>.SET MENU VIEW")
        
        self.addSubview(burgView)
        burgView.addSubview(option1Button)
        burgView.addSubview(option2Button)
//        burgView.addSubview(option3Button)
        
        setMenuConstraints()
        
        inside.addSubview(self)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([

            self.leadingAnchor.constraint(equalTo: inside.leadingAnchor),
            self.trailingAnchor.constraint(equalTo: inside.trailingAnchor),
            self.topAnchor.constraint(equalTo: inside.layoutMarginsGuide.topAnchor),
            self.bottomAnchor.constraint(equalTo: inside.bottomAnchor)
        ])
    }
    
    //MARK: - CONSTRAINTS
    func setMenuConstraints() {
        
        NSLayoutConstraint.activate([
            
            burgView.topAnchor.constraint(equalTo: self.topAnchor),
            burgView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            burgView.bottomAnchor.constraint(equalTo: self.bottomAnchor),
            burgView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.4),

            option1Button.topAnchor.constraint(equalTo: burgView.topAnchor, constant: 10),
            option1Button.trailingAnchor.constraint(equalTo: burgView.trailingAnchor, constant: -10),

            option2Button.topAnchor.constraint(equalTo: option1Button.bottomAnchor, constant: 10),
            option2Button.trailingAnchor.constraint(equalTo: option1Button.trailingAnchor),

//            option3Button.topAnchor.constraint(equalTo: option2Button.bottomAnchor, constant: 10),
//            option3Button.trailingAnchor.constraint(equalTo: option2Button.trailingAnchor)
        ])
    }
    
}

