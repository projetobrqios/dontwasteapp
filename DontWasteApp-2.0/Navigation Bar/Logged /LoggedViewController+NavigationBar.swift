//
//  LoggedViewController+NavigationBar.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit
//MARK: - NAVIGATION BAR
extension LoggedViewController {

    func setNavigationBar(_ withHomeLogged: Bool){
        if withHomeLogged{
            navigationController?
                .isNavigationBarHidden = false

            searchBar.sizeToFit()
            searchBar.delegate = self
            
            showSearchBarButton(shouldShow: true)
        }
        else {
            navigationController?.isNavigationBarHidden = true
        }
    }
    func setButtonConstraints() {
        NSLayoutConstraint.activate([
            
            homeButton.widthAnchor.constraint(equalToConstant: 40),
            homeButton.heightAnchor.constraint(equalToConstant: 38),
            
            burgButton.widthAnchor.constraint(equalToConstant: 40),
            burgButton.heightAnchor.constraint(equalToConstant: 38),
            
            searchButton.widthAnchor.constraint(equalToConstant: 40),
            searchButton.heightAnchor.constraint(equalToConstant: 38),
        ])
    }
    func setNavigationBarTransparent(){
        navigationController?
            .extendedLayoutIncludesOpaqueBars = false
        if let navigationBarSet = navigationController?.navigationBar {
            navigationBarSet.isTranslucent = true
            navigationBarSet.standardAppearance
                .configureWithTransparentBackground()
        }
        else {
            print(">>>>>>>>>> NAV BAR CONFIG não funciona")
        }
    }
}
