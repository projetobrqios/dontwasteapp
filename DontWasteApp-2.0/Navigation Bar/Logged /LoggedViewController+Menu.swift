//
//  LoggedViewController+Menu.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit
import FirebaseAuth
//MARK: - MENU
extension LoggedViewController {
    
    @objc func openMenu() {
        print("=========OPEN MENU PRESSED")
        menuView.setView(inside: self.view)
        //menu targets
        menuView.option1Button.addTarget(
            self,
            action: #selector(goCethegory),
            for: .touchUpInside
        )
        menuView.option2Button.addTarget(
            self,
            action: #selector(logOut),
            for: .touchUpInside
        )
        let tap = UITapGestureRecognizer(
            target: menuView,
            action: #selector(UIView.removeFromSuperview)
        )
        view.addGestureRecognizer(tap)
    }
    //MARK: - button targets
    @objc private func logOut() {
        self.navigationController?.popToRootViewController(animated: true)
        AuthManager().firebaseLogout()
    }
    
    @objc private func goCethegory() {
        print("===========GO CATHEGORY")
        menuView.removeFromSuperview()
        let cadastroCategoriaVC = CathegoriesViewController()
        navigationController?
            .pushViewController(cadastroCategoriaVC, animated: true)
    }
}
