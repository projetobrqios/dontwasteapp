//
//  BlankBasicViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit

class BlankBasicViewController: BasicViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "HOME > BLANK SCREEN"
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
    }
}
