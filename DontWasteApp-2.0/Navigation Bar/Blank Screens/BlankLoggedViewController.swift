//
//  BlankLoggedViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 28/10/21.
//

import UIKit

class BlankLoggedViewController: LoggedViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBar(true)
        setToolBar(true, withLeftButton: true)
        
        title = "LOGGED > BLANK SCREEN"
        
        self.view.backgroundColor = .white
    }
}
