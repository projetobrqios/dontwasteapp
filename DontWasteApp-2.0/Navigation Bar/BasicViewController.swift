//
//  HomeViewController.swift
//  DontWasteApp-2.0
//
//  Created by user on 22/10/21.
//

import UIKit

class BasicViewController: UIViewController {
    
    //MARK: - ATRIBUTES
    private let homeButton = Button()
        .createImageButton(imageName: "cadastrohome")
    
    //MARK: - METHODS
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    //MARK: - navigation bar
    func setNavigationBar(_ withHome: Bool) {
        if withHome {
            navigationController?
                .isNavigationBarHidden = false
            
            setHomeButtonConstraints()
            let homeItem = UIBarButtonItem(customView: homeButton)
            
            navigationItem.leftBarButtonItems = [ homeItem ]
            homeButton.addTarget(
                self,
                action: #selector(goHomeNV),
                for: .touchUpInside
            )
        }
        else {
            navigationController?.isNavigationBarHidden = true
        }
    }
    func setNavigationBarTransparent()
    {
        navigationController?
            .extendedLayoutIncludesOpaqueBars = false
        if let navigationBarSet = navigationController?.navigationBar {
            navigationBarSet.isTranslucent = true
            navigationBarSet.standardAppearance
                .configureWithTransparentBackground()
        }
        else {
            print(">>>>>>>>>> NAV BAR CONFIG não funciona")
        }
    }
    func setHomeButtonConstraints() {
        NSLayoutConstraint.activate([
            homeButton.widthAnchor
                .constraint(equalToConstant: 40),
            homeButton.heightAnchor
                .constraint(equalToConstant: 38)
        ])
    }
    @objc func goHomeNV() {
        print("====================goHome pressed")
        self.setNavigationBarTransparent()
        self.setToolbarTransparent()
        self.navigationController?
            .popToRootViewController(animated: true)
    }
    //MARK: - toolbar
    func setToolBar(
        _ withButtons: Bool,
        withLeftButton: Bool = false,
        withRightButton: Bool = false
    ) {
        if withButtons {
            navigationController?.isToolbarHidden = false
            
            let spacer = UIBarButtonItem(
                barButtonSystemItem: .flexibleSpace,
                target: nil,
                action: nil
            )
            let leftItem = addLeftButton()
            let rightItem = addRightButton()
            
            if withLeftButton && withRightButton {
                toolbarItems = [ leftItem, spacer , rightItem ]
            }
            else if withLeftButton {
                toolbarItems = [ leftItem, spacer ]
            }
            else {
                toolbarItems = [ spacer, rightItem ]
            }
        }
        else {
            navigationController?.isToolbarHidden = true
        }
    }
    func setToolbarTransparent(){
        navigationController?
            .extendedLayoutIncludesOpaqueBars = false
        if let toolbarSet = navigationController?.toolbar {
            toolbarSet.standardAppearance
                .configureWithTransparentBackground()
            toolbarSet.isTranslucent = true
        }
        else {
            print(">>>>>>>>>>TOOLBAR CONFIG não funciona")
        }
    }
    func addLeftButton() -> UIBarButtonItem {
        let backButton = Button()
            .createBottomButton(title: "VOLTAR", green: false)
        let leftItem = UIBarButtonItem(customView: backButton)
        
        backButton.addTarget(
            self,
            action: #selector(goBack),
            for: .touchUpInside
        )
        return leftItem
    }
    func addRightButton() -> UIBarButtonItem {
        let backButton = Button()
            .createBottomButton(title: "VOLTAR", green: true)
        let rightItem = UIBarButtonItem(customView: backButton)
        
        backButton.addTarget(
            self,
            action: #selector(goBack),
            for: .touchUpInside
        )
        return rightItem
    }
    @objc func goBack(){
        print("=================goBack pressed")
        let navArray: [UIViewController] = self.navigationController!.viewControllers as Array
        if navArray[(navArray.count)-2] == HomeViewController()
        {
            self.setNavigationBarTransparent()
            self.setToolbarTransparent()
        }
        navigationController?
            .popViewController(animated: true)
    }
}
